import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatRutPipe } from './format-rut.pipe';

@NgModule({
  declarations: [
    FormatRutPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FormatRutPipe
  ]
})
export class FormatRutModule { }
