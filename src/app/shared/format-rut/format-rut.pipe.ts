import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatRut'
})
export class FormatRutPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    const actual = value.replace(/^0+/, '');
    if (actual !== '' && actual.length > 1) {
        const sinPuntos = actual.replace(/\./g, '');
        const actualLimpio = sinPuntos.replace(/-/g, '');
        const inicio = actualLimpio.substring(0, actualLimpio.length - 1);
        let i = 0;
        let j = 1;
        let rutPuntos = '';
        for (i = inicio.length - 1; i >= 0; i--) {
            const letra = inicio.charAt(i);
            rutPuntos = letra + rutPuntos;
            if (j % 3 === 0 && j <= inicio.length - 1) {
                rutPuntos = '.' + rutPuntos;
            }
            j++;
        }
        const dv = actualLimpio.substring(actualLimpio.length - 1);
        rutPuntos = rutPuntos + '-' + dv;
        return rutPuntos;
    } else {
      return null;
    }
  }

}
