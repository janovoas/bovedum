import * as jwt_decode from 'jwt-decode';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { CardRequestService } from '../../wallet/services/card-request.service';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private accountsUrl = environment.apiUrl + '/accounts';
  public email: string;
  public id: string;

  constructor(
    private http: HttpClient,
    private cardReqService: CardRequestService,
    private authService: AuthService,
  ) {
    this.init();
  }

  init() {
    this.getEmail().subscribe(email => this.email = email);
    this.id = this.getId();
  }

  setAccountMeta(formData: any) {
    const id = this.id;
    return this.http.post<any>(`${this.accountsUrl}/${id}/meta`, formData);
  }

  updateAccountMeta(formData: any) {
    const id = this.id;
    return this.http.put<any>(`${this.accountsUrl}/${id}/meta`, formData);
  }

  getAccountMeta() {
    const id = this.id;
    return this.http.get<any>(`${this.accountsUrl}/${id}/meta`, {observe: 'response'});
  }


  setBankAccount(formData: any) {
    const id = this.id;
    return this.http.post<any>(`${this.accountsUrl}/${id}/bank-account`, formData);
  }

  updateBankAccount(formData: any) {
    const id = this.id;
    return this.http.put<any>(`${this.accountsUrl}/${id}/bank-account`, formData);
  }

  getBankAccount() {
    const id = this.id;
    return this.http.get<any>(`${this.accountsUrl}/${id}/bank-account`, {observe: 'response'});
  }

  getAccountData() {
    const id = this.getId();
    return this.http.get<any>(`${this.accountsUrl}/${id}`);
  }

  setMetalicumPreregister() {
    return this.http.post<any>(`${this.accountsUrl}/${this.id}/metalicum-preregister`, {
      account_id: this.id
    });
  }
  getMetalicumPreregister() {
    return this.http.get<any>(`${this.accountsUrl}/${this.id}/metalicum-preregister`);
  }


  private getEmail() {
    return this.getAccountData().pipe(map((data: any) => data.data.email));
  }

  private getId(): string {
    const TOKEN = this.authService.getToken();
    if (TOKEN) {
      const decoded = jwt_decode(TOKEN);
      const ID  = decoded._id;
      return ID;
    }
    return '0';
  }

  hasCardRequest() {
    const id = this.getId();
    return this.cardReqService.hasCardRequest(id);
  }
}
