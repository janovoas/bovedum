import * as jwt_decode from 'jwt-decode';
import { Account } from './../../shared/classes/account';
import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

export const TOKEN_NAME = 'auth_token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loginUrl = environment.apiUrl + '/accounts/login';
  public authenticated = new BehaviorSubject(null);

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  login(account: Account) {
    return this.http.post<Account>(this.loginUrl, account).subscribe(
      res => {
        const TOKEN = res.token;
        this.setToken(TOKEN);
        this.router.navigate(['wallet']);
        return this.authenticated.next(true);
      },
      err => {
        console.log(err);
      },
      () => {}
    );
  }

  logout() {
    localStorage.removeItem(TOKEN_NAME);
    this.router.navigateByUrl('');
  }

  getToken() {
    return localStorage.getItem(TOKEN_NAME);
  }

  setToken(token: string) {
    localStorage.setItem(TOKEN_NAME, token);
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem(TOKEN_NAME);
    return !this.isTokenExpired(token);
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) {
      return null;
    }

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(TOKEN?: string): boolean {
    if (!TOKEN) {
      TOKEN = this.getToken();
    }
    if (!TOKEN) {
      return true;
    }

    const DATE = this.getTokenExpirationDate(TOKEN);

    if (DATE === undefined) {
      return false;
    }

    return !(DATE.valueOf() > new Date().valueOf());
  }
}
