import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  public countries: Array<object>;

  private countriesUrl = environment.apiUrl + '/countries';

  constructor(
    private http: HttpClient
  ) {
  }

  public getCountries() {
    return this.http.get<any>(this.countriesUrl);
  }
}
