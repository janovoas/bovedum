import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BanksService {
  public banks: Array<object>;

  private banksUrl = environment.apiUrl + '/banks';
  private bankAccountsTypesUrl = environment.apiUrl + '/banks/accounts-types';

  constructor(
    private http: HttpClient
  ) {
  }

  public getBanks() {
    return this.http.get<any>(this.banksUrl);
  }

  public getBankName(id: string) {
    return this.http.get<any>(`${this.banksUrl}/${id}`);
  }

  public getBankAccountTypes() {
    return this.http.get<any>(this.bankAccountsTypesUrl);
  }
}
