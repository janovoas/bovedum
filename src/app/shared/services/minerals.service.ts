import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MineralsService {

  private mineralsUrl = environment.apiUrl + '/minerals';
  private reqsUrl = environment.apiUrl + '/minerals/requests';
  private mineralsPresUrl = environment.apiUrl + '/minerals/presentations';

  // tslint:disable-next-line:variable-name
  private _minerals$: BehaviorSubject<object> = new BehaviorSubject<object>({});
  public readonly minerals$: Observable<any> = this._minerals$.asObservable();

  constructor(
    private http: HttpClient,
  ) {
    this.minerals$ = this.getMinerals();
  }

  public getMinerals() {
   return this.http.get<[]>(this.mineralsUrl);
  }

  public getMineral(mineralId: number) {
    return this.http.get<any>(`${this.mineralsUrl}/${mineralId}`);
  }

  public getMineralsPresentations() {
   return this.http.get<any>(this.mineralsPresUrl, { observe: 'response' });
  }

  public getMineralPresentation(presId: number) {
    return this.http.get<any>(`${this.mineralsPresUrl}/${presId}`);
  }

}
