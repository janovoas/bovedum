export interface Mineral {
    id: string;
    mineral_presentation: string;
    mineral_type: string;
    mineral_name: string;
    mineral_image: string;
    mineral_description: string;
    mineral_value: number;
    total_value: number;
}
