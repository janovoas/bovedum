export class Account {
    id: number;
    email: string;
    pwd: string;
    token: string;
}
