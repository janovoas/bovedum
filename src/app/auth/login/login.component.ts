import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Account } from './../../shared/classes/account';
import { AuthService } from '../../shared/services/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formErrors = [];
  authenticated = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    if (this.authService.authenticated.value === null) {
      this.authService.authenticated.subscribe((res: boolean) => this.authenticated = res);
    }
  }

  ngOnInit() {
  }

  onClickSubmit(account: Account) {
    const { email, pwd } = account;

    if (Object.entries(this.formErrors).length === 0) {
      this.spinner.show();
      this.authService.login(account);
    }
  }

}
