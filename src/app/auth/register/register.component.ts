import { Component, OnInit } from '@angular/core';
import { RegisterService } from './register.service';
import { Router } from '@angular/router';
declare const TOCliveness: any;


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formErrors = [];

  constructor(
    private service: RegisterService,
    private router: Router
  ) { }

  ngOnInit() {
    /* this.service.getAccounts().subscribe(data => {
      console.log(data);
    }); */

    /* if ('geolocation' in navigator) {
      console.log(navigator);
    } */

    /* TOCliveness('liveness',
    {
        locale: 'es',
        session_id: '5262e558f1584cc1a97e813ec04fce95',
        callback: function(token) { alert(token); }
      }
    ); */

  }

  onClickSubmit(formData) {
    if (Object.entries(this.formErrors).length === 0) {
      this.service.createAccount(formData).subscribe(
        (res) => {
          console.log(res);
          this.router.navigateByUrl('');
        },
        (error) => {
          console.error(error);
        },
        () => {
        },
      );
    }
  }

}
