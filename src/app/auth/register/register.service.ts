import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Account } from './../../shared/classes/account';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private accountsUrl = environment.apiUrl + '/accounts';

  constructor(
    private http: HttpClient
  ) { }

  public createAccount(account: Account) {
    return this.http.post(this.accountsUrl, account);
  }

  public updateAccount(account: Account) {
    return this.http.put(`${this.accountsUrl}/${account.id}`, account);
  }

  public deleteAccount(id: number) {
    return this.http.delete(`${this.accountsUrl}/${id}`);
  }

  public getAccountById(id: number) {
    return this.http.get<Account[]>(`${this.accountsUrl}/${id}`);
  }

  public getAccounts(url?: string) {
    return this.http.get<Account[]>(this.accountsUrl);
  }
}
