import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';

import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CheckPasswordDirective } from '../../shared/directives/check-password.directive';

@NgModule({
  declarations: [
    RegisterComponent,
    CheckPasswordDirective
  ],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    FormsModule,
    FontAwesomeModule
  ]
})
export class RegisterModule { }
