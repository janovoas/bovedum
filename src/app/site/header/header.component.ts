import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { AccountService } from '../../shared/services/account.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  authenticated: boolean;
  name: string;

  constructor(
    private authService: AuthService,
    private accService: AccountService
  ) {
    if (this.authService.authenticated.value === null) {
      this.authService.authenticated.subscribe((res: boolean) => this.authenticated = res);
    }
  }

  ngOnInit() {
    if (this.authenticated === null) {
      this.authenticated = this.authService.isAuthenticated();
      if (this.authenticated) {
        this.name = this.accService.email;
      }
    }
  }

  /* get isSidebarVisible(): boolean {
    return this.authService.authenticated;
  } */

  logout() {
    this.authService.logout();
    this.authenticated = false;
  }

}
