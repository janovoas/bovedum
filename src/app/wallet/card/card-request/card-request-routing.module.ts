import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardRequestComponent } from './card-request.component';
import { AuthGuardService } from '../../../shared/guards/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: CardRequestComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardRequestRoutingModule { }
