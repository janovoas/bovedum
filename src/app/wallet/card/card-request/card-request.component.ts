import { Component, OnInit } from '@angular/core';
import { CardRequestService } from '../../services/card-request.service';
import { AccountService } from '../../../shared/services/account.service';

@Component({
  selector: 'app-card-request',
  templateUrl: './card-request.component.html',
  styleUrls: ['./card-request.component.scss']
})
export class CardRequestComponent implements OnInit {
  hasCardRequest = false;

  private id = '';
  accountEmail = '';
  requestPlace: string;

  constructor(
    private reqService: CardRequestService,
    private accService: AccountService,
  ) { }

  ngOnInit() {
    this.id = this.accService.id;

    this.accService.hasCardRequest().subscribe(
      res => {
        this.hasCardRequest = res.body.found;
        if (res.body.hasOwnProperty('data')) {
          this.requestPlace = res.body.data.id;
        }
      },
      err => {
        console.error(err);
      },
      () => {}
    );
  }

  requestCard() {
    const REQUESTDATA = {
      account_id: this.id
    };
    this.reqService.createCardRequest(REQUESTDATA).subscribe(
      (res) => {
        this.hasCardRequest = true;
        this.requestPlace = res.data.id;
      },
      (error) => {
        console.error(error);
      },
      () => {
      },
    );
  }

}
