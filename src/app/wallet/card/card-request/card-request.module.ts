import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardRequestRoutingModule } from './card-request-routing.module';
import { CardRequestComponent } from './card-request.component';

import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WalletHeaderModule } from '../../wallet-header/wallet-header.module';

@NgModule({
  declarations: [CardRequestComponent],
  imports: [
    CommonModule,
    CardRequestRoutingModule,
    FormsModule,
    WalletHeaderModule
  ]
})
export class CardRequestModule { }
