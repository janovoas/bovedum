import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckoutConfirmComponent } from './checkout-confirm.component';

const routes: Routes = [
  {
    path: '',
    component: CheckoutConfirmComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckoutConfirmRoutingModule { }
