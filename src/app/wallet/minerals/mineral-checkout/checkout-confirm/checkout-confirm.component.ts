import { Observable } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CheckoutService } from '../../../services/checkout.service';
import { AccountService } from '../../../../shared/services/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout-confirm',
  templateUrl: './checkout-confirm.component.html',
  styleUrls: ['./checkout-confirm.component.scss']
})
export class CheckoutConfirmComponent implements OnInit, OnDestroy {

  constructor(
    public checkoutService: CheckoutService,
    private accService: AccountService,
    private router: Router
  ) { }

  ngOnInit() {
    if (!this.checkoutService.mineral) {
      this.router.navigate(['wallet']);
    }
    this.checkoutService.setStepTitle('Confirma tu compra');
  }

  ngOnDestroy() {
  }

  requestMineral(paymentType: string) {
    this.checkoutService.requestMineral(paymentType);
  }

}
