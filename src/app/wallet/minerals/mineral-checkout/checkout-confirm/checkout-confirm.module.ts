import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckoutConfirmRoutingModule } from './checkout-confirm-routing.module';
import { CheckoutConfirmComponent } from './checkout-confirm.component';
import { FormatRutModule } from '../../../../shared/format-rut/format-rut.module';
import { PaypalComponent } from '../../../shared/components/paypal/paypal.component';

@NgModule({
  declarations: [
    CheckoutConfirmComponent,
    PaypalComponent
  ],
  imports: [
    CommonModule,
    CheckoutConfirmRoutingModule,
    FormatRutModule,
  ],
  exports: [
    CheckoutConfirmComponent
  ]
})
export class CheckoutConfirmModule { }
