import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MineralCheckoutRoutingModule } from './mineral-checkout-routing.module';
import { MineralCheckoutComponent } from './mineral-checkout.component';
// import { CheckoutService } from '../../services/checkout.service';
import { MineralRequestModule } from './mineral-request/mineral-request.module';
import { RouterModule } from '@angular/router';
import { FormatRutModule } from '../../../shared/format-rut/format-rut.module';
import { MineralPresentationSelectorModule } from './mineral-presentation-selector/mineral-presentation-selector.module';
import { CheckoutConfirmModule } from './checkout-confirm/checkout-confirm.module';
import { ThankyouModule } from './thankyou/thankyou.module';

@NgModule({
  declarations: [
    MineralCheckoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MineralCheckoutRoutingModule,
    MineralRequestModule,
    FormatRutModule,
    MineralPresentationSelectorModule,
    CheckoutConfirmModule,
    ThankyouModule
  ],
  exports: [
    CommonModule,
    MineralCheckoutComponent,
    FormatRutModule
  ],
  // providers: [
  //   CheckoutService
  // ]
})
export class MineralCheckoutModule { }
