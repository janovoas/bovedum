import { Mineral } from './../../../shared/interfaces/mineral';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CheckoutService } from '../../services/checkout.service';
import { take, mergeMap, map, flatMap } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-mineral-checkout',
  templateUrl: './mineral-checkout.component.html',
  styleUrls: ['./mineral-checkout.component.scss']
})
export class MineralCheckoutComponent implements OnInit {

  stepTitle: string;
  step: number;
  mineralType = '';
  mineral: Mineral;

  constructor(
    public checkoutService: CheckoutService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.spinner.show();
    if (!this.checkoutService.mineral) {
      this.router.navigate(['wallet', 'minerals']);
    } else {
      this.checkoutService.getStep()
        .pipe(
          mergeMap(step => {
            this.step = step;
            return this.checkoutService.getTitle();
          }),
          mergeMap(title => {
            this.stepTitle = title;
            return this.checkoutService.getStepTitle();
          })
        )
        .subscribe(stepTitle => {
          this.stepTitle = stepTitle;
          this.mineral = this.checkoutService.mineral;
          this.mineralType = this.mineral.mineral_type;
          this.checkoutService.setMineralType(this.mineralType);
          this.spinner.hide();
        });
    }
  }
}
