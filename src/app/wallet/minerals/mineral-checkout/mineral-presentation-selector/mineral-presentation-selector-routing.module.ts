import { MineralPresentationSelectorComponent } from './mineral-presentation-selector.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: MineralPresentationSelectorComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MineralPresentationSelectorRoutingModule { }
