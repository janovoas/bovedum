import { Component, OnInit } from '@angular/core';
import { CheckoutService } from '../../../services/checkout.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-mineral-presentation-selector',
  templateUrl: './mineral-presentation-selector.component.html',
  styleUrls: ['./mineral-presentation-selector.component.scss']
})
export class MineralPresentationSelectorComponent implements OnInit {
    presentationMin = 50;
    presentationMax = 100;
    step = 50;

    mineralValue: number;
    mineralType: string;
    value: number;

    typesData = ['physical', 'digital'];

  constructor(
    public checkoutService: CheckoutService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    if (!this.checkoutService.mineral) {
      this.router.navigate(['wallet', 'minerals']);
    } else {
      this.checkoutService.setTitle(this.checkoutService.mineral.mineral_name);
      this.checkoutService.setStepTitle('Seleccione la presentación');
      this.mineralValue = this.checkoutService.getMineralValue();
      this.setValues(this.presentationMin);
      this.checkoutService.setMineralType(this.mineralType);
    }
  }

  onInputChange(selectedPresentation) {
    this.setValues(selectedPresentation);
  }

  private setValues(presentation) {
    this.value = this.totalValue(presentation);
    this.checkoutService.setTotalValue(this.value);
    this.checkoutService.setMineralPresentation(presentation);
  }

  private totalValue(presentation) {
    return presentation * this.mineralValue;
  }

  // nextStep() {

  // }

}
