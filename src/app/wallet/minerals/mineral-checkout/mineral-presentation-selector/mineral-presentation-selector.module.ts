import { MatSliderModule } from '@angular/material/slider';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MineralPresentationSelectorComponent } from './mineral-presentation-selector.component';
import { RouterModule } from '@angular/router';
import { MineralPresentationSelectorRoutingModule } from './mineral-presentation-selector-routing.module';


@NgModule({
  declarations: [
    MineralPresentationSelectorComponent
  ],
  imports: [
    CommonModule,
    MatSliderModule,
    RouterModule,
    MineralPresentationSelectorRoutingModule
  ],
  exports: [
    MatSliderModule,
    MineralPresentationSelectorComponent
  ]
})
export class MineralPresentationSelectorModule { }
