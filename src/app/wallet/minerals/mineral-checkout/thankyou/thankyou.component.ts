import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CheckoutService } from '../../../services/checkout.service';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss']
})
export class ThankyouComponent implements OnInit {

  constructor(
    private router: Router,
    public checkoutService: CheckoutService,
  ) { }

  ngOnInit() {
    this.checkoutService.setStepTitle('Orden recibida');
  }

}
