import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThankyouRoutingModule } from './thankyou-routing.module';
import { ThankyouComponent } from './thankyou.component';
import { RouterModule } from '@angular/router';
import { FormatRutModule } from '../../../../shared/format-rut/format-rut.module';

@NgModule({
  declarations: [
    ThankyouComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ThankyouRoutingModule,
    FormatRutModule
  ],
  exports: [
    CommonModule,
    ThankyouComponent
  ]
})
export class ThankyouModule { }
