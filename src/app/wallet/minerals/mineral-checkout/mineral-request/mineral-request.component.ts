import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MineralRequestService } from '../../../services/mineral-request.service';
import { CountriesService } from '../../../../shared/services/countries.service';
import { BanksService } from '../../../../shared/services/banks.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AccountService } from '../../../../shared/services/account.service';
import { trigger, style, animate, transition } from '@angular/animations';
import { CheckoutService } from '../../../services/checkout.service';
import { MatStepper } from '@angular/material/stepper';
import { Router, ActivatedRoute } from '@angular/router';
import { RutValidator } from 'ng2-rut';
import { map, flatMap, mergeMap } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-mineral-request',
  templateUrl: './mineral-request.component.html',
  styleUrls: ['./mineral-request.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({opacity: 0}),
          animate('0.3s ease-in', style({opacity: 1}))
        ]),
        transition(':leave', [
          style({opacity: 1}),
          animate('0s', style({opacity: 0}))
        ])
      ]
    )
  ]
})
export class MineralRequestComponent implements OnInit, AfterViewInit {
  bankFormValid = false;
  dataFormValid = false;
  requestSent = false;
  validRequest = false;
  requestData = {};
  processing = true;

  editingAccountMeta = false;
  editingBankAccount = false;

  hasMeta = false;
  hasBankAccount = false;

  step = 1;
  steps = [
    {},
    {},
    {},
    {},
  ];

  addressForm: FormGroup;
  dataForm: FormGroup;
  bankForm: FormGroup;
  requestForm: FormGroup;

  productTypes: Array<object>;
  productTypeId = '0';
  minerals: Array<object>;
  presentations: Array<object>;
  countries: Array<object>;
  banks = [];
  accountTypes = [];

  @ViewChild('stepper', { static: false }) stepper: MatStepper;

  constructor(
    private reqService: MineralRequestService,
    private accService: AccountService,
    private countriesService: CountriesService,
    private banksService: BanksService,
    private checkoutService: CheckoutService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    rutValidator: RutValidator,
    private spinner: NgxSpinnerService
  ) {
    /* Forms */
    this.dataForm = this.fb.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      second_surname: ['', [Validators.required]],
      country_id: ['', [Validators.required]],
      tel: ['', [Validators.required]],
      rut: ['', [Validators.required, rutValidator]],
    });
    this.addressForm = this.fb.group({
      address: ['', [Validators.required]],
      address_number: ['', [Validators.required]],
      country: ['', [Validators.required]],
      city: ['', [Validators.required]],
      zip: ['', [Validators.required]],
    });
    this.bankForm = this.fb.group({
      bank_id: ['', [Validators.required]],
      bank_account_type_id: ['', [Validators.required]],
      bank_account_number: ['', [Validators.required]],
      bank_account_rut: ['', [Validators.required, rutValidator]]
    });
    this.requestForm = this.fb.group({
      request_sell_conditions: ['', [Validators.requiredTrue]],
      request_deposit_contract: ['', [Validators.requiredTrue]],
      request_custody_service: ['', [Validators.requiredTrue]],
    });
  }


  ngOnInit() {
    this.spinner.show();

    this.checkoutService.setStepTitle('Checkout');
    if (!this.checkoutService.mineral || !this.checkoutService.mineral.total_value) {
      this.router.navigate(['wallet', 'products']);
    }

    this.reqService.getMinerals().subscribe(
      res => {
        this.minerals = res.body.data;
      }
    );

    this.reqService.getMineralsPresentations().subscribe(
      res => {
        this.presentations = res.body.data;
      }
    );

    this.countriesService.getCountries().subscribe(
      countries => {
        this.countries = countries.data.map(country => {
          return {
            id: country.id,
            country_name: country.country_name
          };
        });
      }
    );
    this.banksService.getBanks().subscribe(
      banks => {
        this.banks = banks.data.map(bank => {
          return {
            id: bank.id,
            bank_name: bank.bank_name
          };
        });
      }
    );
    this.banksService.getBankAccountTypes().subscribe(
      types => {
        this.accountTypes = types.data.map(type => {
          return {
            id: type.id,
            account_type: type.account_type
          };
        });
      }
    );

  }

  ngAfterViewInit() {
    this.checkoutService.getAccountMeta()
      .pipe(
        mergeMap(r => {
          if (r.status === 200) {
            for (const key in this.dataForm.controls) {
              if (r.body.data.hasOwnProperty(key) && this.dataForm.controls.hasOwnProperty(key)) {
                const element = this.dataForm.controls[key];
                element.setValue(r.body.data[key]);
              }
            }
            this.hasMeta = true;
            if (!this.dataForm.valid) {
              // this.stepperIndex = 0;
            } else {
              this.dataFormValid = true;
            }
          }
          return this.checkoutService.getBankAccount();
        })
      )
      .subscribe(r => {
        if (r.status === 200) {
          for (const key in this.bankForm.controls) {
            if (r.body.data.hasOwnProperty(key) && this.bankForm.controls.hasOwnProperty(key)) {
              const element = this.bankForm.get(key);
              element.setValue(r.body.data[key]);
            }
          }
          this.hasBankAccount = true;
          if (!this.bankForm.valid) {
            // this.stepperIndex = 1;
          } else {
            this.bankFormValid = true;
            // this.stepperIndex = 2;
          }
        }
        this.processing = false;
        this.spinner.hide();
      });
  }

  setAccountMeta() {
    const FORMDATA = {
      ... this.dataForm.value
    };
    if (this.hasMeta) {
      this.accService.updateAccountMeta(FORMDATA).subscribe(
        r => {
          // console.log(r);
        },
        e => {
          console.log(e);
        }
      );
    } else {
      this.accService.setAccountMeta(FORMDATA).subscribe(
        r => {
          this.hasMeta = true;
        },
        e => {
          console.log(e);
        }
      );
     }
    this.nextStep();
  }

  setBankAccount() {
    const FORMDATA = {
      ... this.bankForm.value
    };
    if (this.hasBankAccount) {
      this.accService.updateBankAccount(FORMDATA).subscribe(
        r => {
          // console.log(r);
        },
        e => {
          console.log(e);
        }
      );
    } else {
      this.accService.setBankAccount(FORMDATA).subscribe(
        r => {
          this.hasBankAccount = true;
        },
        e => {
          console.log(e);
        }
      );
     }
    this.nextStep();
  }

  setRequestData() {
    const FORMDATA = {
      requestData: {
        account_email: this.accService.email,
        ... this.addressForm.value,
        ... this.bankForm.value,
      },
      checkoutData: {
        account: {
          name: `${this.dataForm.get('name').value} ${this.dataForm.get('surname').value} ${this.dataForm.get('second_surname').value}`,
          rut: this.dataForm.get('rut').value,
          account_email: this.accService.email,
        },
        bank: {
          name: this.banks[(this.bankForm.get('bank_id').value) - 1].bank_name,
          accountType: this.accountTypes[(this.bankForm.get('bank_account_type_id').value) - 1].account_type,
          accountNumber: this.bankForm.get('bank_account_number').value,
          accountRut: this.bankForm.get('bank_account_rut').value,
        },
        address: this.addressForm.value,
      }
    };
    this.checkoutService.setRequestData(FORMDATA);
  }

  nextStep() {
    if (this.step < 5) {
      this.step = this.step + 1;
    }
  }
  prevStep() {
    if (this.step > 0) {
      this.step = this.step - 1;
    }
  }

}
