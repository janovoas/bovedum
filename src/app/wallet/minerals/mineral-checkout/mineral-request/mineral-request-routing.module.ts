import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MineralRequestComponent } from './mineral-request.component';

const routes: Routes = [
  {
    path: '',
    component: MineralRequestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MineralRequestRoutingModule { }
