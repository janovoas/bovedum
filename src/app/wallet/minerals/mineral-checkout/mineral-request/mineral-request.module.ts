import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MineralRequestRoutingModule } from './mineral-request-routing.module';
import { MineralRequestComponent } from './mineral-request.component';
import { CdkStepperModule } from '@angular/cdk/stepper';

import { MatStepperModule } from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';


import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Ng2Rut } from 'ng2-rut';

@NgModule({
  declarations: [
    MineralRequestComponent
  ],
  imports: [
    CommonModule,
    MineralRequestRoutingModule,
    FormsModule,
    CdkStepperModule,
    MatStepperModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    Ng2Rut
  ],
  exports: [
    MineralRequestComponent
  ]
})
export class MineralRequestModule { }
