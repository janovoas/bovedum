import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MineralCheckoutComponent } from './mineral-checkout.component';
import { AuthGuardService } from '../../../shared/guards/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: MineralCheckoutComponent,
    canActivate: [AuthGuardService],
    children: [
      // {
      //   path: 'presentation',
        // tslint:disable-next-line:max-line-length
      //   loadChildren: () => import('./mineral-presentation-selector/mineral-presentation-selector.module').then(m => m.MineralPresentationSelectorModule)
      // },
      // {
      //   path: 'presentation/data',
      //   loadChildren: () => import('./mineral-request/mineral-request.module').then(m => m.MineralRequestModule)
      // },
      // {
      //   path: 'presentation/data/confirm',
      //   loadChildren: () => import('./checkout-confirm/checkout-confirm.module').then(m => m.CheckoutConfirmModule)
      // },
      {
        path: 'thankyou',
        loadChildren: () => import('./thankyou/thankyou.module').then(m => m.ThankyouModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MineralCheckoutRoutingModule { }
