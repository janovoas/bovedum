import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MetalicumRoutingModule } from './metalicum-routing.module';
import { MetalicumComponent } from './metalicum.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    MetalicumComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MetalicumRoutingModule
  ],
  exports: [
    CommonModule,
    MetalicumComponent
  ]
})
export class MetalicumModule { }
