import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MetalicumComponent } from './metalicum.component';

const routes: Routes = [
  {
    path: '',
    component: MetalicumComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MetalicumRoutingModule { }
