import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/services/auth.service';
import { AccountService } from '../../../shared/services/account.service';

@Component({
  selector: 'app-metalicum',
  templateUrl: './metalicum.component.html',
  styleUrls: ['./metalicum.component.scss']
})
export class MetalicumComponent implements OnInit {

  hasPreregister: boolean;

  constructor(
    private authService: AuthService,
    private accService: AccountService
  ) { }

  ngOnInit() {
    this.accService.getMetalicumPreregister().subscribe(
      hasPreregister => {
        this.hasPreregister = hasPreregister.success;
      }
    );
  }

  metalicumRegister() {
    this.accService.setMetalicumPreregister().subscribe(
      response => {
        if (response.success) {
          this.hasPreregister = true;
        }
      }
    );
  }

}
