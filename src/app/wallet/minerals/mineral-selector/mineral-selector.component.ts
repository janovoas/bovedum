import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MineralsService } from 'src/app/shared/services/minerals.service';
import { CheckoutService } from '../../services/checkout.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-mineral-selector',
  templateUrl: './mineral-selector.component.html',
  styleUrls: ['./mineral-selector.component.scss']
})
export class MineralSelectorComponent implements OnInit {
  mineralType: string;

  products = [];
  step = 0;

  typesData = {
    physical: {
      title: '¿Por qué minerales?',
      description: 'Obtenga beneficios reales de su capital. Compra de forma fácil oro certificado y asegura tu patrimonio'
    },
    digital: {
      title: '¿Por qué minerales digitales?',
      description: 'Obtenga beneficios reales de su capital. Compra de forma fácil oro certificado y asegura tu patrimonio'
    }
  };

  constructor(
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router,
    private checkoutService: CheckoutService,
    private mineralService: MineralsService,
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.checkoutService.reset();
    this.mineralService.minerals$.subscribe(
      response => {
        this.products = response.minerals.sort((a: any, b: any) => (a.id > b.id) ? 1 : -1);
        this.products
          .map((mineral) => {
            if (mineral.mineral_name !== 'Metalicum') {
              mineral.btn = 'Comprar';
              mineral.btn_url = `checkout`;
              const VALWITHMARGIN = mineral.mineral_value + 2;
              const PROFIT = VALWITHMARGIN * 0.02;
              mineral.mineral_value = VALWITHMARGIN + PROFIT;
            } else {
              mineral.btn = 'Me interesa';
              mineral.btn_url = 'metalicum';
            }
          });
        this.spinner.hide();
      }
    );
  }

  setMineral(mineral) {
    this.checkoutService.setMineral(mineral);
  }

}
