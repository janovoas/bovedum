import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../../shared/guards/auth-guard.service';
import { MineralSelectorComponent } from './mineral-selector.component';

const routes: Routes = [
  {
    path: '',
    component: MineralSelectorComponent,
    canActivate: [AuthGuardService],
  },
  {  path: '**', redirectTo: 'wallet' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MineralSelectorRoutingModule { }
