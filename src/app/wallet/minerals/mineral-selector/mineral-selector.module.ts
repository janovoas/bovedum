import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatExpansionModule } from '@angular/material/expansion';
import { MineralSelectorComponent } from './mineral-selector.component';
import { MineralSelectorRoutingModule } from './mineral-selector-routing.module';

@NgModule({
  declarations: [
    MineralSelectorComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MineralSelectorRoutingModule,
    MatExpansionModule
  ],
  exports: [
    CommonModule,
    MineralSelectorComponent,
  ]
})
export class MineralSelectorModule { }
