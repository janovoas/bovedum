import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MineralsComponent } from './minerals.component';
import { AuthGuardService } from '../../shared/guards/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: MineralsComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        loadChildren: () => import('./mineral-selector/mineral-selector.module').then(m => m.MineralSelectorModule)
      },
      // {
      //   path: ':type/checkout',
      //   loadChildren: () => import('./mineral-checkout/mineral-checkout.module').then(m => m.MineralCheckoutModule)
      // },
      {
        path: 'checkout',
        loadChildren: () => import('./mineral-checkout/mineral-checkout.module').then(m => m.MineralCheckoutModule)
      },
      {
        path: 'metalicum',
        loadChildren: () => import('./metalicum/metalicum.module').then(m => m.MetalicumModule)
      },
    ]
  },
  {  path: '**', redirectTo: 'wallet' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MineralsRoutingModule { }
