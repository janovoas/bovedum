import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MineralsRoutingModule } from './minerals-routing.module';
import { MineralsComponent } from './minerals.component';
import { RouterModule } from '@angular/router';
import { MineralCheckoutModule } from './mineral-checkout/mineral-checkout.module';
import { CheckoutService } from '../services/checkout.service';
import { MatExpansionModule } from '@angular/material/expansion';
import { MineralSelectorModule } from './mineral-selector/mineral-selector.module';

@NgModule({
  declarations: [
    MineralsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MineralsRoutingModule,
    MineralCheckoutModule,
    MineralSelectorModule,
    MatExpansionModule,
  ],
  exports: [
    CommonModule,
    MineralsComponent,
  ],
  providers: [
    CheckoutService
  ]
})
export class MineralsModule { }
