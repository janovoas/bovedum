import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WalletDashboardRoutingModule } from './wallet-dashboard-routing.module';
import { WalletDashboardComponent } from './wallet-dashboard.component';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [WalletDashboardComponent],
  imports: [
    CommonModule,
    RouterModule,
    ChartsModule
  ],
  exports: [
    CommonModule,
    WalletDashboardComponent,
    WalletDashboardRoutingModule
  ],
})
export class WalletDashboardModule { }
