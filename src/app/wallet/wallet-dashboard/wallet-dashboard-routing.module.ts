import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WalletDashboardComponent } from './wallet-dashboard.component';
import { AuthGuardService } from '../../shared/guards/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: WalletDashboardComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WalletDashboardRoutingModule { }
