import { Component, OnInit, ViewChild } from '@angular/core';
import { MineralRequestService } from '../services/mineral-request.service';
import { MineralsService } from '../../shared/services/minerals.service';
import { map, flatMap, switchMap, mergeMap } from 'rxjs/operators';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { AccountService } from '../../shared/services/account.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-wallet-dashboard',
  templateUrl: './wallet-dashboard.component.html',
  styleUrls: ['./wallet-dashboard.component.scss']
})
export class WalletDashboardComponent implements OnInit {

  requests = [];
  hasCardRequest = false;
  requestPlace: string;

  troyOzUsd: number;
  troyOzClp: number;

  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
  ];
  public lineChartLabels: Label[] = ['', '', '', '', '', '', ''];
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
  ];
  public lineChartLegend = false;
  public lineChartType = 'line';
  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(
    private spinner: NgxSpinnerService,
    private mineralReqService: MineralRequestService,
    private mineralsService: MineralsService,
    private accService: AccountService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.mineralsService.getMineral(1).subscribe(
      mineral => {
        const VALWITHMARGIN = mineral.data.mineral_value + 2;
        const PROFIT = VALWITHMARGIN * 0.02;
        const TROYUSD = ((VALWITHMARGIN + PROFIT) * 31.1);
        const TROYCLP = TROYUSD * 700;
        this.troyOzUsd = TROYUSD;
        this.troyOzClp = TROYCLP;
      }
    );

    this.accService.hasCardRequest().subscribe(
      res => {
        this.hasCardRequest = res.body.found;
        if (res.body.hasOwnProperty('data')) {
          this.requestPlace = res.body.data.id;
        }
      },
      err => {
        console.error(err);
      },
      () => {}
    );

    this.mineralReqService.getMineralsRequests()
    .subscribe(
      response => {
        this.requests = response.body.data;
        this.spinner.hide();
      }
    );
  }

}
