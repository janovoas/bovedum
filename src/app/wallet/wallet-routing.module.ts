import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../shared/guards/auth-guard.service';
import { WalletComponent } from './wallet.component';

const routes: Routes = [
  {
    path: '',
    component: WalletComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        loadChildren: () => import('./wallet-dashboard/wallet-dashboard.module').then(m => m.WalletDashboardModule)
      },
      {
        path: 'card',
        loadChildren: () => import('./card/card-request/card-request.module').then(m => m.CardRequestModule)
      },
      {
        path: 'minerals',
        loadChildren: () => import('./minerals/minerals.module').then(m => m.MineralsModule)
      },
      {  path: '**', redirectTo: '' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WalletRoutingModule { }
