export interface Paypal {
    cart: string;
    create_time: string;
    id: string;
    intent: string;
    payer: {
        payer_info: {
            country_code: string,
            email: string,
            first_name: string,
            last_name: string,
            middle_name: string,
            payer_id: string,
            shipping_address: {
                city: string,
                country_code: string,
                line1: string,
                postal_code: string,
                recipient_name: string,
                state: string
            }
        }
        payment_method: string,
        status: string
    };
    state: string;
    transactions: Array<object>;
}
