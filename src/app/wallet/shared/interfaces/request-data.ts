export interface RequestData {
    payment_type: string;
    account_id: string;
    mineral_id: string;
    mineral_presentation: string;
    total_value: number;
    status_id: number;
}
