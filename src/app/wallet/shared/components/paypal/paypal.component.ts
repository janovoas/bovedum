import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { CheckoutService } from '../../../services/checkout.service';

declare let paypal: any;

@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.scss']
})
export class PaypalComponent implements OnInit, AfterViewChecked {
  addScript = false;
  paypalLoad = false;
  total = 0;

  paypalConfig = {
    env: 'sandbox',
    client: {
      sandbox: 'AUTn62Qvvb9d6I3evISUwqpfIi2Zbjfe8-Bs7KYw-aNh2YI9NPHFSHikHXOAz-fVgz-BdJ29lIF9L8nE',
      production: '<your-production-key here>'
    },
    style: {
      size: 'large'
    },
    commit: true,
    payment: (data, actions) => {
      return actions.payment.create({
        payment: {
          transactions: [
            {
              amount: {
                total: this.total,
                currency: 'USD'
              }
            }
          ]
        }
      });
    },
    onAuthorize: (data, actions) => {
      return actions.payment.execute().then(
        (payment) => {
          if (payment.state === 'approved') {
            this.checkoutService.setPayPalData(payment);
            this.checkoutService.requestMineral('paypal');
          }
        }
      );
    },
  };

  constructor(
    private checkoutService: CheckoutService
  ) { }

  ngOnInit() {
    this.total = this.checkoutService.getTotalValue();
  }

  setTotal(total: number) {
    this.total = total;
  }

  ngAfterViewChecked(): void {
    if (!this.addScript) {
      this.addPaypalScript().then(() => {
        paypal.Button.render(this.paypalConfig, '#paypal-checkout-btn');
        this.paypalLoad = false;
      });
    }
  }

  addPaypalScript() {
    this.addScript = true;
    const URL = 'https://www.paypalobjects.com/api/checkout.js';
    return new Promise((resolve, reject) => {
      const scripttagElement = document.createElement('script');
      scripttagElement.src = URL;
      scripttagElement.id = 'paypal-script';
      scripttagElement.onload = resolve;
      document.body.appendChild(scripttagElement);
    });
  }

}
