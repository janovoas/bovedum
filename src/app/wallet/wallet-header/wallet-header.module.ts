import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WalletHeaderComponent } from './wallet-header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [WalletHeaderComponent],
  imports: [
    CommonModule,
    NgbModule,
    FontAwesomeModule,
    RouterModule
  ],
  exports: [
    CommonModule,
    WalletHeaderComponent,
    FontAwesomeModule
  ],
})
export class WalletHeaderModule { }
