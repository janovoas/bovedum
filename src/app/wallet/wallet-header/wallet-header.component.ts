import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { AccountService } from '../../shared/services/account.service';

@Component({
  selector: 'app-wallet-header',
  templateUrl: './wallet-header.component.html',
  styleUrls: ['./wallet-header.component.scss']
})
export class WalletHeaderComponent implements OnInit {

  public isCollapsed = true;

  authenticated = false;
  name: string;

  constructor(
    private authService: AuthService,
    private accService: AccountService
  ) { }

  ngOnInit() {
    if (!this.authenticated) {
      this.authenticated = this.authService.isAuthenticated();
      if (this.authenticated) {
        // this.name = this.accService.email;
      }
    }
  }

  logOut() {
    this.authService.logout();
  }

}
