import { AccountService } from '../../shared/services/account.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MineralRequestService {

  private id;

  private mineralsUrl = environment.apiUrl + '/minerals';
  private reqsUrl = environment.apiUrl + '/minerals/requests';
  private mineralsPresUrl = environment.apiUrl + '/minerals/presentations';

  constructor(
    private http: HttpClient,
    private accService: AccountService,
  ) {
    this.id = this.accService.id;
  }

  public getMineralsRequests() {
    return this.http.get<any>(this.reqsUrl + '/' + this.id, { observe: 'response' });
  }
  public setMineralRequest(formData) {
    return this.http.post<any>(this.reqsUrl, formData, { observe: 'response' });
  }

  public getMinerals() {
   return this.http.get<any>(this.mineralsUrl, { observe: 'response' });
  }
  public getMineralsPresentations() {
   return this.http.get<any>(this.mineralsPresUrl, { observe: 'response' });
  }
}
