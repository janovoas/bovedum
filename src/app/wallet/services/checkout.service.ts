import { RequestData } from './../shared/interfaces/request-data';
import { Paypal } from './../shared/interfaces/paypal';
import { Mineral } from './../../shared/interfaces/mineral';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { AccountService } from '../../shared/services/account.service';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { MineralRequestService } from './mineral-request.service';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  public requestSent: boolean;
  public requestSentChange$: Subject<boolean> = new Subject<boolean>();

  // tslint:disable-next-line:variable-name
  public _title$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public readonly title$: Observable<string> = this._title$.asObservable();

  // tslint:disable-next-line:variable-name
  public _stepTitle$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public readonly stepTitle$: Observable<string> = this._stepTitle$.asObservable();

  // tslint:disable-next-line:variable-name
  private _step$: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  public readonly step$: Observable<number> = this._step$.asObservable();
  private step = 1;

  private mineralsUrl = environment.apiUrl + '/minerals';
  private mineralsPresUrl = environment.apiUrl + '/minerals/presentations';

  private accountId: string;

  public mineralType: string;

  public mineralValue: number;
  public mineral: Mineral;

  // tslint:disable-next-line:variable-name
  private _checkoutData$: BehaviorSubject<object> = new BehaviorSubject({});
  public readonly checkoutData$: Observable<object> = this._checkoutData$.asObservable();
  private checkoutData: any;

  private requestData: RequestData;

  constructor(
    private http: HttpClient,
    private accService: AccountService,
    private mineralReqService: MineralRequestService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.setAccountId();
    this.requestSentChange$.subscribe((value) => {
      this.requestSent = value;
    });
    this.requestSentChange$.next(false);
    this._step$.next(this.step);
  }

  private setAccountId() {
    this.accountId = this.accService.id;
  }

  public setMineral(mineral) {
    this.mineral = mineral;
  }

  public setMineralType(mineralType: string) {
    this.mineralType = mineralType;
  }
  public getMineralType() {
    return this.mineralType;
  }

  public setMineralPresentation(mineralPresentationId) {
    this.mineral.mineral_presentation = mineralPresentationId;
  }
  public getMineralPresentation() {
    return this.mineral.mineral_presentation;
  }

  public setMineralValue(value: number) {
    this.mineral.mineral_value = value;
  }
  public getMineralValue(): number {
    return this.mineral.mineral_value;
  }

  public getSelectedMineral() {
    if (this.mineral) {
      return this.mineral;
    }
    return false;
  }

  public getMinerals() {
   return this.http.get<[]>(this.mineralsUrl);
  }
  public getMineralsPresentations() {
   return this.http.get<any>(this.mineralsPresUrl, { observe: 'response' });
  }

  public setTotalValue(value: number) {
    this.mineral.total_value = value;
  }
  public getTotalValue(): number {
    return this.mineral.total_value;
  }

  getAccountMeta() {
    return this.accService.getAccountMeta();
  }
  getBankAccount() {
    return this.accService.getBankAccount();
  }

  public getStepTitle() {
    return this.stepTitle$;
  }
  public setStepTitle(stepTitle: string) {
    this._stepTitle$.next(stepTitle);
  }

  public nextStep() {
    this.step = this.step + 1;
    this._step$.next(this.step);
  }
  public prevStep() {
    this.step = this.step - 1;
    this._step$.next(this.step);
  }
  public getStep() {
    return this.step$;
  }

  public getTitle() {
    return this.title$;
  }
  public setTitle(title: string) {
    this._title$.next(title);
  }

  public getCheckoutData() {
    return this.checkoutData$;
  }

  public setRequestData(FORMDATA) {
    FORMDATA.checkoutData.transaction = {
      transactionId: 0,
      mineralName: this.mineral.mineral_name,
      mineralQuantity: this.mineral.mineral_presentation,
      mineralValue: this.getMineralValue(),
      totalValue: this.getTotalValue(),
      date: new Date(),
    };
    FORMDATA.requestData.mineral_name = this.mineral.mineral_name;

    this.checkoutData = FORMDATA.checkoutData;
    this.requestData = FORMDATA.requestData;
    this._checkoutData$.next(this.checkoutData);
    this.nextStep();
  }

  /* Set PayPal data */
  public setPayPalData(payPalData: Paypal) {
    this.checkoutData.paypal = payPalData;
    this._checkoutData$.next(this.checkoutData);
  }

  public getPaymentType() {
    return this.requestData.payment_type;
  }

  /**
   * Request Mineral
   * @param paymentType Payment type
   */
  public requestMineral(paymentType: string) {
    this.requestData.payment_type = paymentType;
    this.requestData.account_id = this.accountId;
    this.requestData.mineral_id = this.mineral.id;
    this.requestData.mineral_presentation = this.mineral.mineral_presentation;
    this.requestData.total_value = this.mineral.total_value;

    if (this.requestData.payment_type === 'transfer') {
      this.requestData.status_id = 1;
    } else {
      this.requestData.status_id = 2;
    }

    this.mineralReqService.setMineralRequest(this.requestData).subscribe(
      (res: any) => {
        if (res.body.success) {
          this.requestSentChange$.next(res.body.success);
          this.checkoutData.transaction.transactionId = res.body.data.id;
          this.nextStep();
        }
      },
      err => {
        console.log(err);
      },
      () => {}
    );
  }

  reset() {
    if (document.getElementById('paypal-script')) {
      document.getElementById('paypal-script').remove();
    }
    this.requestData = {} as RequestData;
    this.step = 1;
    this._step$.next(this.step);
  }
}
