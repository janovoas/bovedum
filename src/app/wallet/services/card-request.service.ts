import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CardRequestService {

  private reqsUrl = environment.apiUrl + '/card-requests';

  // Form Data
  email = '';

  constructor(
    private http: HttpClient
  ) { }

  public createCardRequest(data: object) {
    return this.http.post<any>(this.reqsUrl, data);
  }

  public hasCardRequest(id: string) {
    return this.http.get<any>(this.reqsUrl + '/' + id, { observe: 'response' });
  }
}
