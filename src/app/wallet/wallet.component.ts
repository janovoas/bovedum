import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AccountService } from '../shared/services/account.service';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit, AfterViewInit {

  constructor(
    private spinner: NgxSpinnerService,
    private accService: AccountService,
  ) { }

  ngOnInit() {
    this.accService.getAccountMeta().subscribe(
      r => {
        // console.log(r);
      }
    );
  }

  ngAfterViewInit() {
    // this.spinner.hide();
  }

}
