import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountService } from '../shared/services/account.service';

import { WalletRoutingModule } from './wallet-routing.module';

import { WalletComponent } from './wallet.component';

import { WalletHeaderModule } from './wallet-header/wallet-header.module';

import { WalletDashboardModule } from './wallet-dashboard/wallet-dashboard.module';
import { WalletMenuComponent } from './wallet-menu/wallet-menu.component';

@NgModule({
  declarations: [
    WalletComponent,
    WalletMenuComponent,
  ],
  imports: [
    CommonModule,
    WalletRoutingModule,
    WalletHeaderModule,
    WalletDashboardModule,
  ],
  providers: [
    AccountService
  ]
})
export class WalletModule { }
