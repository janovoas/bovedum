'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    /* return queryInterface.addColumn('bv_accounts', 'lastName', {
      type: Sequelize.STRING
    }); */

    return queryInterface.addColumn('bv_mineral_requests', 'account_id', {
      type: Sequelize.BIGINT,
      references: {
        model: "bv_accounts",
        key: "id"
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    /* return queryInterface.removeColumn('bv_accounts', 'lastName', {
      type: Sequelize.STRING
    }); */
  }
};
