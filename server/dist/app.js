"use strict";

var _express = _interopRequireWildcard(require("express"));

var _morgan = _interopRequireDefault(require("morgan"));

var _ddbbCron = require("./crons/ddbb.cron.js");

var _config = require("./config");

var _accounts = _interopRequireDefault(require("./routes/accounts"));

var _wallet = _interopRequireDefault(require("./routes/wallet"));

var _cardRequests = _interopRequireDefault(require("./routes/card-requests"));

var _mineralsRequests = _interopRequireDefault(require("./routes/minerals-requests"));

var _minerals = _interopRequireDefault(require("./routes/minerals"));

var _mineralsPresentations = _interopRequireDefault(require("./routes/minerals-presentations"));

var _banks = _interopRequireDefault(require("./routes/banks"));

var _bankAccountsTypes = _interopRequireDefault(require("./routes/bank-accounts-types"));

var _countries = _interopRequireDefault(require("./routes/countries"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var cors = require("cors");

var path = require('path');

var checkToken = require('./routes/middlewares/checkToken');

var APPURL = _config.CONFVARS[_config.ENV].appUrl;
(0, _ddbbCron.deleteTenMnts)();
/* Importing Routes */

/* Init */
var app = (0, _express.default)();

if (_config.ENV === 'prod') {
  var distDir = path.join(__dirname, '../..', '/dist/');
  app.use(_express.default.static(distDir));
}
/* Middlewares */


var corsOptions = function corsOptions(req, res, next) {
  var WHITELIST = [APPURL, 'https://app.bovedum.com'];
  var ORIGIN = req.headers.origin;

  if (WHITELIST.indexOf(ORIGIN) > -1) {
    res.header('Access-Control-Allow-Origin', ORIGIN);
  }

  res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin,X-Requested-With,Content-Type,Accept,X-Access-Token,Authorization');
  /* allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token", "Authorization"],
  credentials: true,
  methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
  origin: APPURL,
  preflightContinue: false */

  next();
};

app.use(corsOptions); // app.use(morgan('dev')); // Mostrar por consola las peticiones

app.use((0, _express.json)());
/* Routes */
// Accounts

app.use('/api/accounts', _accounts.default); // Card requests

app.use('/api/card-requests', _cardRequests.default); // Minerals requests

app.use('/api/minerals/requests', _mineralsRequests.default); // Wallet
// app.use('/wallet', walletRoutes);
// Minerals

app.use('/api/minerals/presentations', _mineralsPresentations.default);
app.use('/api/minerals', _minerals.default); // Bank Accounts from account

app.use('/api/banks/accounts-types', _bankAccountsTypes.default);
app.use('/api/banks', _banks.default); // Countries

app.use('/api/countries', _countries.default);
app.use('/*', function (req, res) {
  res.sendfile(path.join(__dirname, '../..', '/dist/index.html'));
});
var MODULEXPORT = {
  app: app,
  httpsOptions: {}
};

if (_config.ENV === 'dev') {
  var _CONFVARS$ENV = _config.CONFVARS[_config.ENV],
      key = _CONFVARS$ENV.key,
      cert = _CONFVARS$ENV.cert;
  MODULEXPORT.httpsOptions = {
    key: key,
    cert: cert
  };
} // export default app;


module.exports = MODULEXPORT;