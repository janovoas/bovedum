"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _mineral = require("../controllers/mineral.controller");

var router = (0, _express.Router)();
/* Get */

router.get('/:id', _mineral.getMineral);
router.get('/', _mineral.getMinerals);
/* Login */

/* Create */
// router.post('/', createMineral);

/* Delete */
// router.delete('/:id', deleteMineral);

var _default = router;
exports.default = _default;