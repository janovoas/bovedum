"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _mineralsRequest = require("../controllers/minerals-request.controller");

var router = (0, _express.Router)();
/* Get */

router.get('/:id', _mineralsRequest.getUserMineralRequests);
router.get('/', _mineralsRequest.getMineralsRequests);
/* Create */

router.post('/', _mineralsRequest.createMineralRequest);
/* Delete */

router.delete('/:id', _mineralsRequest.deleteMineralRequest);
var _default = router;
exports.default = _default;