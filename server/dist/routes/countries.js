"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _country = require("../controllers/country.controller");

var router = (0, _express.Router)();
/* Get */

router.get('/:id', _country.getCountry);
router.get('/', _country.getCountries);
/* Login */

/* Create */
// router.post('/', createMineralPresentation);

/* Delete */
// router.delete('/:id', deleteMineralPresentation);

var _default = router;
exports.default = _default;