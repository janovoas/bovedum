"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _account = require("../controllers/account.controller");

var checkToken = require('../routes/middlewares/checkToken');

var router = (0, _express.Router)();
/* Account */

router.get('/:id', checkToken.checkToken, _account.getAccount);
router.get('/', checkToken.checkToken, _account.getAccounts);
router.post('/', _account.createAccount);
/* Login */

router.post('/login', _account.loginAccount);
/* Personal Info */

router.post('/:id/meta', checkToken.checkToken, _account.setAccountMeta);
router.get('/:id/meta', checkToken.checkToken, _account.getAccountMeta);
router.put('/:id/meta', checkToken.checkToken, _account.updateAccountMeta);
/* Metalicum Preregister */

router.post('/:id/metalicum-preregister', checkToken.checkToken, _account.setMetalicumPreregister);
router.get('/:id/metalicum-preregister', checkToken.checkToken, _account.getMetalicumPreregister);
/* Bank Accounts */

router.post('/:id/bank-account', checkToken.checkToken, _account.setBankAccount);
router.get('/:id/bank-account', checkToken.checkToken, _account.getBankAccount);
router.put('/:id/bank-account', checkToken.checkToken, _account.updateBankAccount);
/* Delete */

router.delete('/:id', checkToken.checkToken, _account.deleteAccount);
var _default = router;
exports.default = _default;