"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _cardRequest = require("../controllers/card-request.controller");

var checkToken = require('../routes/middlewares/checkToken');

var router = (0, _express.Router)();
/* Get */

router.get('/:id', checkToken.checkToken, _cardRequest.getCardRequest);
router.get('/', checkToken.checkToken, _cardRequest.getCardRequests);
/* Create */

router.post('/', checkToken.checkToken, _cardRequest.createCardRequest);
/* Delete */

router.delete('/:id', checkToken.checkToken, _cardRequest.deleteCardRequest);
var _default = router;
exports.default = _default;