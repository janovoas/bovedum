"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CONFVARS = exports.ENV = void 0;

var crypto = require('crypto');

var fs = require("fs");

var ENV = process.env.ENV || 'dev';
exports.ENV = ENV;
var KEYPATH = "/usr/local/etc/httpd/ssl/server.key";
var CERTPATH = "/usr/local/etc/httpd/ssl/server.crt";
var key, cert;

if (fs.existsSync(KEYPATH)) {
  key = fs.readFileSync(KEYPATH, 'utf-8');
}

if (fs.existsSync(CERTPATH)) {
  cert = fs.readFileSync(CERTPATH, 'utf-8');
}

var CONFVARS = {
  secret: crypto.randomBytes(256).toString('base64'),
  dev: {
    appUrl: 'https://localhost:4200',
    key: key,
    cert: cert,
    DDBBNAME: 'pruebas',
    USER: 'jn',
    PWD: 'adm',
    HOST: '127.0.0.1',
    PORT: 5432
  },
  prod: {
    appUrl: 'https://dev-bovedum.herokuapp.com/',
    DDBBNAME: 'd3mqtfj659f6dc',
    USER: 'ltvrfrmxkanhaz',
    PWD: 'd54a0eaa0806393fb82d73397a964a2a05348c32423ebf793192dc782c45f499',
    HOST: 'ec2-54-225-196-93.compute-1.amazonaws.com',
    PORT: 5432
  }
};
exports.CONFVARS = CONFVARS;