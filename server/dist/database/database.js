"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sequelize = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _config = require("../config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _CONFVARS$ENV = _config.CONFVARS[_config.ENV],
    DDBBNAME = _CONFVARS$ENV.DDBBNAME,
    USER = _CONFVARS$ENV.USER,
    PWD = _CONFVARS$ENV.PWD,
    HOST = _CONFVARS$ENV.HOST,
    PORT = _CONFVARS$ENV.PORT;
var sequelize = new _sequelize.default(DDBBNAME, USER, PWD, {
  host: HOST,
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    require: 30000,
    idle: 10000
  },
  logging: false,
  hooks: {
    beforeDefine: function beforeDefine(columns, model) {
      model.tableName = 'bv_' + model.name.plural;
    }
  }
});
exports.sequelize = sequelize;