"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAccounts = getAccounts;
exports.getAccount = getAccount;
exports.createAccount = createAccount;
exports.loginAccount = loginAccount;
exports.deleteAccount = deleteAccount;
exports.getAccountMeta = getAccountMeta;
exports.setAccountMeta = setAccountMeta;
exports.updateAccountMeta = updateAccountMeta;
exports.setMetalicumPreregister = setMetalicumPreregister;
exports.getMetalicumPreregister = getMetalicumPreregister;
exports.setBankAccount = setBankAccount;
exports.updateBankAccount = updateBankAccount;
exports.getBankAccount = getBankAccount;

var _Account = _interopRequireDefault(require("../models/Account"));

var _AccountMeta = _interopRequireDefault(require("../models/AccountMeta"));

var _BankAccount = _interopRequireDefault(require("../models/BankAccount"));

var _bcryptjs = _interopRequireDefault(require("bcryptjs"));

var _config = require("../config");

var _MetalicumPreregister = _interopRequireDefault(require("../models/MetalicumPreregister"));

var _sendgrid = require("../mail/sendgrid");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var jwt = require('jsonwebtoken');

/* Read */
function getAccounts(_x, _x2) {
  return _getAccounts.apply(this, arguments);
}

function _getAccounts() {
  _getAccounts = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(req, res) {
    var accounts;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _Account.default.findAll({
              attributes: ['id', 'email']
            });

          case 3:
            accounts = _context.sent;
            res.json({
              data: accounts
            });
            _context.next = 10;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getAccounts.apply(this, arguments);
}

function getAccount(_x3, _x4) {
  return _getAccount.apply(this, arguments);
}
/* Create */


function _getAccount() {
  _getAccount = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(req, res) {
    var id, account;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            id = req.params.id;
            _context2.next = 4;
            return _Account.default.findOne({
              where: {
                id: id
              },
              attributes: ['id', 'email']
            });

          case 4:
            account = _context2.sent;

            if (!account) {
              _context2.next = 9;
              break;
            }

            return _context2.abrupt("return", res.json({
              data: account
            }));

          case 9:
            return _context2.abrupt("return", res.status(404).json({
              message: "Account ID #".concat(id, " not found")
            }));

          case 10:
            _context2.next = 15;
            break;

          case 12:
            _context2.prev = 12;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 12]]);
  }));
  return _getAccount.apply(this, arguments);
}

function createAccount(_x5, _x6) {
  return _createAccount.apply(this, arguments);
}

function _createAccount() {
  _createAccount = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3(req, res) {
    var _req$body, email, password, SALT, newAccount, msg, msgHowWorks, errorName, errorCode, errorDetail, errorMessage, _e$parent;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _req$body = req.body, email = _req$body.email, password = _req$body.password;
            _context3.next = 3;
            return _bcryptjs.default.genSalt(10);

          case 3:
            SALT = _context3.sent;
            _context3.next = 6;
            return _bcryptjs.default.hash(password, SALT);

          case 6:
            req.body.password = _context3.sent;
            _context3.prev = 7;
            _context3.next = 10;
            return _Account.default.create(req.body, {
              fields: ["email", "password"]
            });

          case 10:
            newAccount = _context3.sent;

            if (!newAccount) {
              _context3.next = 17;
              break;
            }

            msg = {
              to: req.body.email,
              from: 'Bovedum<contacto@bovedum.com>',
              subject: 'Bienvenido a Bovedum',
              templateId: 'd-e5e6c0601fe3475eb54609b3630d1e5b'
            };
            msgHowWorks = {
              to: req.body.email,
              from: 'Bovedum<contacto@bovedum.com>',
              subject: '¿Cómo funciona Bovedum?',
              templateId: 'd-271e0f7bf1bd4d67bf39a7c600a21a7f'
            };
            (0, _sendgrid.sendMail)(msg);
            (0, _sendgrid.sendMail)(msgHowWorks);
            return _context3.abrupt("return", res.status(201).json({
              message: 'Account created',
              data: newAccount
            }));

          case 17:
            _context3.next = 24;
            break;

          case 19:
            _context3.prev = 19;
            _context3.t0 = _context3["catch"](7);
            errorMessage = "";

            if (_context3.t0.hasOwnProperty("parent")) {
              _e$parent = _context3.t0.parent;
              errorName = _e$parent.severity;
              errorCode = _e$parent.code;
              errorDetail = _e$parent.detail;
              errorMessage = "".concat(errorName, " (").concat(errorCode, "): ").concat(errorDetail);
            } else if (_context3.t0.hasOwnProperty("errors")) {
              errorMessage = [];

              _context3.t0.errors.forEach(function (element) {
                errorName = element.type;
                errorCode = element.validatorKey;
                errorDetail = element.message;
                errorMessage.push("".concat(errorName, " (").concat(errorCode, "): ").concat(errorDetail));
              });
            }

            res.status(500).json({
              errorMessage: errorMessage
            });

          case 24:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[7, 19]]);
  }));
  return _createAccount.apply(this, arguments);
}

;
/* Login */

function loginAccount(_x7, _x8) {
  return _loginAccount.apply(this, arguments);
}
/* Delete */


function _loginAccount() {
  _loginAccount = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee4(req, res) {
    var _req$body2, email, password, account, VALIDPASS, TOKEN;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _req$body2 = req.body, email = _req$body2.email, password = _req$body2.password; // Check that account exists

            _context4.next = 4;
            return _Account.default.findOne({
              where: {
                email: email
              }
            });

          case 4:
            account = _context4.sent;

            if (account) {
              _context4.next = 7;
              break;
            }

            return _context4.abrupt("return", res.status(404).json({
              message: "Account email ".concat(email, " not found"),
              data: {}
            }));

          case 7:
            _context4.next = 9;
            return _bcryptjs.default.compare(password, account.password);

          case 9:
            VALIDPASS = _context4.sent;

            if (VALIDPASS) {
              _context4.next = 12;
              break;
            }

            return _context4.abrupt("return", res.status(404).json({
              message: "Account password wrong",
              data: {}
            }));

          case 12:
            // Logged in
            TOKEN = jwt.sign({
              _id: account.id,
              email: account.email
            }, _config.CONFVARS.secret, {
              expiresIn: 600
            });
            res.header('auth-token', TOKEN).json({
              message: 'Logged in',
              token: TOKEN
            });
            _context4.next = 20;
            break;

          case 16:
            _context4.prev = 16;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);
            return _context4.abrupt("return", res.status(500).json({
              message: 'Error'
            }));

          case 20:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 16]]);
  }));
  return _loginAccount.apply(this, arguments);
}

function deleteAccount(_x9, _x10) {
  return _deleteAccount.apply(this, arguments);
}

function _deleteAccount() {
  _deleteAccount = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee5(req, res) {
    var id, deleteRowCount;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            id = req.params.id;
            _context5.next = 4;
            return _Account.default.destroy({
              where: {
                id: id
              }
            });

          case 4:
            deleteRowCount = _context5.sent;

            if (deleteRowCount > 0) {
              res.json({
                message: "Account ID #".concat(id, " deleted"),
                count: deleteRowCount
              });
            } else {
              res.status(500).json({
                message: "Account ID #".concat(id, " not found")
              });
            }

            _context5.next = 11;
            break;

          case 8:
            _context5.prev = 8;
            _context5.t0 = _context5["catch"](0);
            console.log(_context5.t0);

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 8]]);
  }));
  return _deleteAccount.apply(this, arguments);
}

function getAccountMeta(_x11, _x12) {
  return _getAccountMeta.apply(this, arguments);
}
/* Create */


function _getAccountMeta() {
  _getAccountMeta = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee6(req, res) {
    var id, ACCOUNTMETA;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            id = req.params.id;
            _context6.next = 4;
            return _AccountMeta.default.findOne({
              where: {
                account_id: id
              }
            });

          case 4:
            ACCOUNTMETA = _context6.sent;

            if (!ACCOUNTMETA) {
              _context6.next = 9;
              break;
            }

            return _context6.abrupt("return", res.status(200).json({
              data: ACCOUNTMETA
            }));

          case 9:
            return _context6.abrupt("return", res.status(204).json({
              message: "Account ID #".concat(id, " not found")
            }));

          case 10:
            _context6.next = 15;
            break;

          case 12:
            _context6.prev = 12;
            _context6.t0 = _context6["catch"](0);
            console.log(_context6.t0);

          case 15:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 12]]);
  }));
  return _getAccountMeta.apply(this, arguments);
}

function setAccountMeta(_x13, _x14) {
  return _setAccountMeta.apply(this, arguments);
}

function _setAccountMeta() {
  _setAccountMeta = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee7(req, res) {
    var DATA, newAccountMeta, errorName, errorCode, errorDetail, errorMessage, _e$parent2;

    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            DATA = _objectSpread({
              account_id: req.params.id
            }, req.body);
            _context7.next = 4;
            return _AccountMeta.default.create(DATA, {
              fields: ["account_id", "name", "surname", "second_surname", "tel", "country_id", "rut"]
            });

          case 4:
            newAccountMeta = _context7.sent;

            if (!newAccountMeta) {
              _context7.next = 7;
              break;
            }

            return _context7.abrupt("return", res.status(201).json({
              message: 'Account Meta created',
              data: newAccountMeta
            }));

          case 7:
            _context7.next = 15;
            break;

          case 9:
            _context7.prev = 9;
            _context7.t0 = _context7["catch"](0);
            console.log(_context7.t0);
            errorMessage = "";

            if (_context7.t0.hasOwnProperty("parent")) {
              _e$parent2 = _context7.t0.parent;
              errorName = _e$parent2.severity;
              errorCode = _e$parent2.code;
              errorDetail = _e$parent2.detail;
              errorMessage = "".concat(errorName, " (").concat(errorCode, "): ").concat(errorDetail);
            } else if (_context7.t0.hasOwnProperty("errors")) {
              errorMessage = [];

              _context7.t0.errors.forEach(function (element) {
                errorName = element.type;
                errorCode = element.validatorKey;
                errorDetail = element.message;
                errorMessage.push("".concat(errorName, " (").concat(errorCode, "): ").concat(errorDetail));
              });
            }

            res.status(500).json({
              e: _context7.t0
            });

          case 15:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 9]]);
  }));
  return _setAccountMeta.apply(this, arguments);
}

;

function updateAccountMeta(_x15, _x16) {
  return _updateAccountMeta.apply(this, arguments);
}

function _updateAccountMeta() {
  _updateAccountMeta = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee8(req, res) {
    var ACCOUNTID, PARAMS, UPDATED;
    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            ACCOUNTID = req.params.id;
            PARAMS = req.body;
            UPDATED = _AccountMeta.default.update(PARAMS, {
              returning: true,
              where: {
                account_id: ACCOUNTID
              }
            });

            if (!UPDATED) {
              _context8.next = 8;
              break;
            }

            return _context8.abrupt("return", res.status(200).json({
              success: true
            }));

          case 8:
            return _context8.abrupt("return", res.status(500).json({
              data: UPDATED,
              success: false
            }));

          case 9:
            _context8.next = 14;
            break;

          case 11:
            _context8.prev = 11;
            _context8.t0 = _context8["catch"](0);
            res.status(500).json({
              error: _context8.t0,
              success: false
            });

          case 14:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[0, 11]]);
  }));
  return _updateAccountMeta.apply(this, arguments);
}

;
/* Metalicum Preregister */

function setMetalicumPreregister(_x17, _x18) {
  return _setMetalicumPreregister.apply(this, arguments);
}

function _setMetalicumPreregister() {
  _setMetalicumPreregister = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee9(req, res) {
    var newMetalicumPreregister;
    return regeneratorRuntime.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.prev = 0;
            _context9.next = 3;
            return _MetalicumPreregister.default.create(req.body, {
              fields: ["account_id"]
            });

          case 3:
            newMetalicumPreregister = _context9.sent;

            if (!newMetalicumPreregister) {
              _context9.next = 6;
              break;
            }

            return _context9.abrupt("return", res.status(201).json({
              success: true
            }));

          case 6:
            _context9.next = 11;
            break;

          case 8:
            _context9.prev = 8;
            _context9.t0 = _context9["catch"](0);
            res.json({
              success: false
            });

          case 11:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9, null, [[0, 8]]);
  }));
  return _setMetalicumPreregister.apply(this, arguments);
}

;

function getMetalicumPreregister(_x19, _x20) {
  return _getMetalicumPreregister.apply(this, arguments);
}

function _getMetalicumPreregister() {
  _getMetalicumPreregister = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee10(req, res) {
    var id, ACCOUNTMETA;
    return regeneratorRuntime.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.prev = 0;
            id = req.params.id;
            _context10.next = 4;
            return _MetalicumPreregister.default.findOne({
              where: {
                account_id: id
              }
            });

          case 4:
            ACCOUNTMETA = _context10.sent;

            if (!ACCOUNTMETA) {
              _context10.next = 9;
              break;
            }

            return _context10.abrupt("return", res.json({
              success: true
            }));

          case 9:
            return _context10.abrupt("return", res.json({
              success: false
            }));

          case 10:
            _context10.next = 15;
            break;

          case 12:
            _context10.prev = 12;
            _context10.t0 = _context10["catch"](0);
            return _context10.abrupt("return", res.status(500).json({
              message: _context10.t0,
              success: false
            }));

          case 15:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10, null, [[0, 12]]);
  }));
  return _getMetalicumPreregister.apply(this, arguments);
}

;
/* Bank Accounts */

function setBankAccount(_x21, _x22) {
  return _setBankAccount.apply(this, arguments);
}

function _setBankAccount() {
  _setBankAccount = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee11(req, res) {
    var DATA, newBankAccount;
    return regeneratorRuntime.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            DATA = _objectSpread({
              account_id: req.params.id
            }, req.body);
            _context11.prev = 1;
            _context11.next = 4;
            return _BankAccount.default.create(DATA, {
              fields: ["account_id", "bank_account_number", "bank_account_rut", "bank_id", "bank_account_type_id"]
            });

          case 4:
            newBankAccount = _context11.sent;

            if (!newBankAccount) {
              _context11.next = 7;
              break;
            }

            return _context11.abrupt("return", res.status(201).json({
              success: true
            }));

          case 7:
            _context11.next = 12;
            break;

          case 9:
            _context11.prev = 9;
            _context11.t0 = _context11["catch"](1);
            res.json({
              success: false
            });

          case 12:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11, null, [[1, 9]]);
  }));
  return _setBankAccount.apply(this, arguments);
}

;

function updateBankAccount(_x23, _x24) {
  return _updateBankAccount.apply(this, arguments);
}

function _updateBankAccount() {
  _updateBankAccount = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee12(req, res) {
    var ACCOUNTID, PARAMS, UPDATED;
    return regeneratorRuntime.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.prev = 0;
            ACCOUNTID = req.params.id;
            PARAMS = req.body;
            UPDATED = _BankAccount.default.update(PARAMS, {
              returning: true,
              where: {
                account_id: ACCOUNTID
              }
            });

            if (!UPDATED) {
              _context12.next = 8;
              break;
            }

            return _context12.abrupt("return", res.status(200).json({
              success: true
            }));

          case 8:
            return _context12.abrupt("return", res.status(500).json({
              data: UPDATED,
              success: false
            }));

          case 9:
            _context12.next = 14;
            break;

          case 11:
            _context12.prev = 11;
            _context12.t0 = _context12["catch"](0);
            res.status(500).json({
              error: _context12.t0,
              success: false
            });

          case 14:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12, null, [[0, 11]]);
  }));
  return _updateBankAccount.apply(this, arguments);
}

;

function getBankAccount(_x25, _x26) {
  return _getBankAccount.apply(this, arguments);
}

function _getBankAccount() {
  _getBankAccount = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee13(req, res) {
    var id, BANKACCOUNT;
    return regeneratorRuntime.wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            _context13.prev = 0;
            id = req.params.id;
            _context13.next = 4;
            return _BankAccount.default.findOne({
              where: {
                account_id: id
              }
            });

          case 4:
            BANKACCOUNT = _context13.sent;

            if (!BANKACCOUNT) {
              _context13.next = 9;
              break;
            }

            return _context13.abrupt("return", res.status(200).json({
              data: BANKACCOUNT,
              success: true
            }));

          case 9:
            return _context13.abrupt("return", res.status(204).json({
              success: false
            }));

          case 10:
            _context13.next = 15;
            break;

          case 12:
            _context13.prev = 12;
            _context13.t0 = _context13["catch"](0);
            return _context13.abrupt("return", res.status(500).json({
              message: _context13.t0,
              success: false
            }));

          case 15:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13, null, [[0, 12]]);
  }));
  return _getBankAccount.apply(this, arguments);
}

;