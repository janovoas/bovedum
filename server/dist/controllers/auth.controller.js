"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAuths = getAuths;
exports.getAuth = getAuth;
exports.createAuth = createAuth;
exports.deleteAuth = deleteAuth;

var _Accounts = _interopRequireDefault(require("../models/Accounts"));

var _bcryptjs = _interopRequireDefault(require("bcryptjs"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* Read */
function getAuths(_x, _x2) {
  return _getAuths.apply(this, arguments);
}

function _getAuths() {
  _getAuths = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(req, res) {
    var auths;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return Auth.findAll();

          case 3:
            auths = _context.sent;
            res.json({
              data: auths
            });
            _context.next = 10;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getAuths.apply(this, arguments);
}

function getAuth(_x3, _x4) {
  return _getAuth.apply(this, arguments);
}
/* Create */


function _getAuth() {
  _getAuth = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(req, res) {
    var id, auth;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            id = req.params.id;
            _context2.next = 4;
            return Auth.findOne({
              where: {
                id: id
              }
            });

          case 4:
            auth = _context2.sent;

            if (!auth) {
              _context2.next = 9;
              break;
            }

            return _context2.abrupt("return", res.json({
              data: auth
            }));

          case 9:
            return _context2.abrupt("return", res.status(404).json({
              message: "Auth ID #".concat(id, " not found"),
              data: {}
            }));

          case 10:
            _context2.next = 15;
            break;

          case 12:
            _context2.prev = 12;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 12]]);
  }));
  return _getAuth.apply(this, arguments);
}

function createAuth(_x5, _x6) {
  return _createAuth.apply(this, arguments);
}

function _createAuth() {
  _createAuth = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3(req, res) {
    var _req$body, email, password, country_id, SALT, HASHPASSWORD, newAuth, errorName, errorCode, errorDetail, errorMessage, _e$parent;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _req$body = req.body, email = _req$body.email, password = _req$body.password, country_id = _req$body.country_id;
            _context3.next = 3;
            return _bcryptjs.default.genSalt(10);

          case 3:
            SALT = _context3.sent;
            _context3.next = 6;
            return _bcryptjs.default.hash(password, SALT);

          case 6:
            HASHPASSWORD = _context3.sent;
            _context3.prev = 7;
            _context3.next = 10;
            return Auth.create({
              email: email,
              password: HASHPASSWORD,
              country_id: country_id
            });

          case 10:
            newAuth = _context3.sent;

            if (!newAuth) {
              _context3.next = 13;
              break;
            }

            return _context3.abrupt("return", res.json({
              message: 'Auth created',
              data: newAuth
            }));

          case 13:
            _context3.next = 20;
            break;

          case 15:
            _context3.prev = 15;
            _context3.t0 = _context3["catch"](7);
            errorMessage = "";

            if (_context3.t0.hasOwnProperty("parent")) {
              _e$parent = _context3.t0.parent;
              errorName = _e$parent.severity;
              errorCode = _e$parent.code;
              errorDetail = _e$parent.detail;
              errorMessage = "".concat(errorName, " (").concat(errorCode, "): ").concat(errorDetail);
            } else if (_context3.t0.hasOwnProperty("errors")) {
              errorMessage = [];

              _context3.t0.errors.forEach(function (element) {
                errorName = element.type;
                errorCode = element.validatorKey;
                errorDetail = element.message;
                errorMessage.push("".concat(errorName, " (").concat(errorCode, "): ").concat(errorDetail));
              });
            }

            res.status(500).json({
              errorMessage: errorMessage
            });

          case 20:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[7, 15]]);
  }));
  return _createAuth.apply(this, arguments);
}

;
/* Delete */

function deleteAuth(_x7, _x8) {
  return _deleteAuth.apply(this, arguments);
}

function _deleteAuth() {
  _deleteAuth = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee4(req, res) {
    var id, deleteRowCount;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            id = req.params.id;
            _context4.next = 4;
            return Auth.destroy({
              where: {
                id: id
              }
            });

          case 4:
            deleteRowCount = _context4.sent;

            if (deleteRowCount > 0) {
              res.json({
                message: "Auth ID #".concat(id, " deleted"),
                count: deleteRowCount
              });
            } else {
              res.status(500).json({
                message: "Auth ID #".concat(id, " not found")
              });
            }

            _context4.next = 11;
            break;

          case 8:
            _context4.prev = 8;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 8]]);
  }));
  return _deleteAuth.apply(this, arguments);
}