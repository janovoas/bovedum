"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getCardRequests = getCardRequests;
exports.getCardRequest = getCardRequest;
exports.createCardRequest = createCardRequest;
exports.deleteCardRequest = deleteCardRequest;

var _CardRequest = _interopRequireDefault(require("../models/CardRequest"));

var _config = require("../config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* Read */
function getCardRequests(_x, _x2) {
  return _getCardRequests.apply(this, arguments);
}

function _getCardRequests() {
  _getCardRequests = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(req, res) {
    var requests;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _CardRequest.default.findAll();

          case 3:
            requests = _context.sent;
            res.json({
              data: requests
            });
            _context.next = 10;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getCardRequests.apply(this, arguments);
}

function getCardRequest(_x3, _x4) {
  return _getCardRequest.apply(this, arguments);
}
/* Create */


function _getCardRequest() {
  _getCardRequest = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(req, res) {
    var account_id, request;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            account_id = req.params.id;
            _context2.next = 4;
            return _CardRequest.default.findOne({
              where: {
                account_id: account_id
              }
            });

          case 4:
            request = _context2.sent;

            if (!request) {
              _context2.next = 9;
              break;
            }

            return _context2.abrupt("return", res.json({
              found: true,
              data: request
            }));

          case 9:
            return _context2.abrupt("return", res.json({
              found: false,
              message: "CardRequest from account ID #".concat(account_id, " not found")
            }));

          case 10:
            _context2.next = 15;
            break;

          case 12:
            _context2.prev = 12;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 12]]);
  }));
  return _getCardRequest.apply(this, arguments);
}

function createCardRequest(_x5, _x6) {
  return _createCardRequest.apply(this, arguments);
}

function _createCardRequest() {
  _createCardRequest = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3(req, res) {
    var account_id, REQUESTDATA, newCardRequest, errorName, errorCode, errorDetail, errorMessage, _e$parent, response;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            account_id = req.body.account_id;
            REQUESTDATA = {
              account_id: account_id,
              status_id: 1
            };
            _context3.prev = 2;
            _context3.next = 5;
            return _CardRequest.default.create(REQUESTDATA, {
              fields: ["account_id", "status_id"]
            });

          case 5:
            newCardRequest = _context3.sent;

            if (!newCardRequest) {
              _context3.next = 8;
              break;
            }

            return _context3.abrupt("return", res.json({
              message: 'CardRequest created',
              data: newCardRequest,
              success: true
            }));

          case 8:
            _context3.next = 16;
            break;

          case 10:
            _context3.prev = 10;
            _context3.t0 = _context3["catch"](2);
            errorMessage = "";

            if (_context3.t0.hasOwnProperty("parent")) {
              _e$parent = _context3.t0.parent;
              errorName = _e$parent.severity;
              errorCode = _e$parent.code;
              errorDetail = _e$parent.detail;
              errorMessage = "".concat(errorName, " (").concat(errorCode, "): ").concat(errorDetail);
            } else if (_context3.t0.hasOwnProperty("errors")) {
              errorMessage = [];

              _context3.t0.errors.forEach(function (element) {
                errorName = element.type;
                errorCode = element.validatorKey;
                errorDetail = element.message;
                errorMessage.push("".concat(errorName, " (").concat(errorCode, "): ").concat(errorDetail));
              });
            }

            response = {
              message: errorMessage,
              success: false
            };
            res.status(500).json(response);

          case 16:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[2, 10]]);
  }));
  return _createCardRequest.apply(this, arguments);
}

;
/* Delete */

function deleteCardRequest(_x7, _x8) {
  return _deleteCardRequest.apply(this, arguments);
}

function _deleteCardRequest() {
  _deleteCardRequest = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee4(req, res) {
    var id, deleteRowCount;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            id = req.params.id;
            _context4.next = 4;
            return _CardRequest.default.destroy({
              where: {
                id: id
              }
            });

          case 4:
            deleteRowCount = _context4.sent;

            if (deleteRowCount > 0) {
              res.json({
                message: "CardRequest ID #".concat(id, " deleted"),
                count: deleteRowCount
              });
            } else {
              res.status(500).json({
                message: "CardRequest ID #".concat(id, " not found")
              });
            }

            _context4.next = 11;
            break;

          case 8:
            _context4.prev = 8;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 8]]);
  }));
  return _deleteCardRequest.apply(this, arguments);
}