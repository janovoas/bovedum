"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMineralsRequests = getMineralsRequests;
exports.getUserMineralRequests = getUserMineralRequests;
exports.createMineralRequest = createMineralRequest;
exports.deleteMineralRequest = deleteMineralRequest;

var _MineralRequest = _interopRequireDefault(require("../models/MineralRequest"));

var _Mineral = require("../models/info/Mineral");

var _sendgrid = require("../mail/sendgrid");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* Read */
function getMineralsRequests(_x, _x2) {
  return _getMineralsRequests.apply(this, arguments);
}

function _getMineralsRequests() {
  _getMineralsRequests = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(req, res) {
    var requests;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _MineralRequest.default.findAll({
              include: [{
                model: _Mineral.Mineral
              }]
            });

          case 3:
            requests = _context.sent;
            res.json({
              data: requests
            });
            _context.next = 10;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getMineralsRequests.apply(this, arguments);
}

function getUserMineralRequests(_x3, _x4) {
  return _getUserMineralRequests.apply(this, arguments);
}
/* Create */


function _getUserMineralRequests() {
  _getUserMineralRequests = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(req, res) {
    var account_id, request;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            account_id = req.params.id;
            _context2.next = 4;
            return _MineralRequest.default.findAll({
              include: [{
                model: _Mineral.Mineral
              }],
              where: {
                account_id: account_id
              },
              attributes: {
                exclude: ['account_id']
              }
            });

          case 4:
            request = _context2.sent;

            if (!request) {
              _context2.next = 9;
              break;
            }

            return _context2.abrupt("return", res.json({
              found: true,
              data: request
            }));

          case 9:
            return _context2.abrupt("return", res.json({
              found: false,
              message: "MineralRequest from account ID #".concat(account_id, " not found")
            }));

          case 10:
            _context2.next = 15;
            break;

          case 12:
            _context2.prev = 12;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 12]]);
  }));
  return _getUserMineralRequests.apply(this, arguments);
}

function createMineralRequest(_x5, _x6) {
  return _createMineralRequest.apply(this, arguments);
}

function _createMineralRequest() {
  _createMineralRequest = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3(req, res) {
    var newMineralRequest, formatter, ORDERID, USERID, TOTALVALUE, MINERAL, MINERALPRES, msg, msgAdmin, errorName, errorCode, errorDetail, errorMessage, _e$parent, response;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return _MineralRequest.default.create(req.body, {
              fields: ["address", "address_number", "city", "country", "zip", "bank_account_number", "bank_account_rut", "mineral_id", "mineral_name", "mineral_presentation", "mineral_presentation_id", "bank_id", "bank_account_type_id", "name", "surname", "second_surname", "rut", "total_value", "account_id", "status_id", "payment_type"]
            });

          case 3:
            newMineralRequest = _context3.sent;

            if (!newMineralRequest) {
              _context3.next = 13;
              break;
            }

            formatter = new Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
              minimumFractionDigits: 2
            });
            ORDERID = newMineralRequest.id;
            USERID = req.body.account_id;
            TOTALVALUE = formatter.format(req.body.total_value);
            MINERAL = req.body.mineral_name;
            MINERALPRES = req.body.mineral_presentation;

            if (process.env.ENV !== 'dev') {
              msg = {
                to: req.body.account_email,
                from: 'Bovedum<contacto@bovedum.com>',
                templateId: 'd-7fd77cd43f9a491db2b832bd20737f77',
                dynamic_template_data: {
                  subject: "Nueva solicitud de mineral (orden #".concat(ORDERID, ")"),
                  ORDERID: ORDERID,
                  USERID: USERID,
                  TOTALVALUE: TOTALVALUE,
                  MINERAL: MINERAL,
                  MINERALPRES: MINERALPRES
                }
              };
              (0, _sendgrid.sendMail)(msg);
              msgAdmin = {
                to: 'nicolas@bovedum.com',
                cc: 'javier@metalicum.com',
                from: 'Bovedum<contacto@bovedum.com>',
                templateId: 'd-c1d4d1c84ce041749d9fa9f3ec19badf',
                dynamic_template_data: {
                  subject: "Solicitud de producto - #".concat(newMineralRequest.dataValues.id),
                  ORDERID: ORDERID,
                  USERID: USERID,
                  TOTALVALUE: TOTALVALUE,
                  MINERAL: MINERAL,
                  MINERALPRES: MINERALPRES
                }
              };
              (0, _sendgrid.sendMail)(msgAdmin);
            }

            return _context3.abrupt("return", res.json({
              message: 'MineralRequest created',
              data: newMineralRequest,
              success: true
            }));

          case 13:
            _context3.next = 21;
            break;

          case 15:
            _context3.prev = 15;
            _context3.t0 = _context3["catch"](0);
            errorMessage = "";

            if (_context3.t0.hasOwnProperty("parent")) {
              _e$parent = _context3.t0.parent;
              errorName = _e$parent.severity;
              errorCode = _e$parent.code;
              errorDetail = _e$parent.detail;
              errorMessage = "".concat(errorName, " (").concat(errorCode, "): ").concat(errorDetail);
            } else if (_context3.t0.hasOwnProperty("errors")) {
              errorMessage = [];

              _context3.t0.errors.forEach(function (element) {
                errorName = element.type;
                errorCode = element.validatorKey;
                errorDetail = element.message;
                errorMessage.push("".concat(errorName, " (").concat(errorCode, "): ").concat(errorDetail));
              });
            }

            response = {
              message: _context3.t0,
              success: false
            };
            res.status(500).json(response);

          case 21:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 15]]);
  }));
  return _createMineralRequest.apply(this, arguments);
}

;
/* Delete */

function deleteMineralRequest(_x7, _x8) {
  return _deleteMineralRequest.apply(this, arguments);
}

function _deleteMineralRequest() {
  _deleteMineralRequest = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee4(req, res) {
    var id, deleteRowCount;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            id = req.params.id;
            _context4.next = 4;
            return _MineralRequest.default.destroy({
              where: {
                id: id
              }
            });

          case 4:
            deleteRowCount = _context4.sent;

            if (deleteRowCount > 0) {
              res.json({
                message: "MineralRequest ID #".concat(id, " deleted"),
                count: deleteRowCount
              });
            } else {
              res.status(500).json({
                message: "MineralRequest ID #".concat(id, " not found")
              });
            }

            _context4.next = 11;
            break;

          case 8:
            _context4.prev = 8;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 8]]);
  }));
  return _deleteMineralRequest.apply(this, arguments);
}