"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getBankAccountTypes = getBankAccountTypes;
exports.getBankAccountType = getBankAccountType;

var _AccountType = require("../models/info/AccountType");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* Read */
function getBankAccountTypes(_x, _x2) {
  return _getBankAccountTypes.apply(this, arguments);
}

function _getBankAccountTypes() {
  _getBankAccountTypes = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(req, res) {
    var bankaccounttypes;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _AccountType.BankAccountType.findAll();

          case 3:
            bankaccounttypes = _context.sent;
            res.status(200).json({
              data: bankaccounttypes
            });
            _context.next = 10;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getBankAccountTypes.apply(this, arguments);
}

function getBankAccountType(_x3, _x4) {
  return _getBankAccountType.apply(this, arguments);
}
/* Create */
// export async function createMineral(req, res) {
//     const { email, password } = req.body;
//     const SALT = await bcrypt.genSalt(10);
//     req.body.password = await bcrypt.hash(password, SALT);
//     try {
//         let newMineral = await Mineral.create(req.body, {
//             fields: ["email", "password"]
//         });
//         if (newMineral) {
//             return res.json({
//                 message: 'Mineral created',
//                 data: newMineral
//             });
//         }        
//     } catch (e) {
//         let errorName, errorCode, errorDetail, errorMessage = "";
//         if (e.hasOwnProperty("parent")) {
//             ({severity:errorName, code: errorCode, detail: errorDetail} = e.parent);
//             errorMessage = `${errorName} (${errorCode}): ${errorDetail}`;
//         } else if(e.hasOwnProperty("errors")) {
//             errorMessage = [];
//             e.errors.forEach(element => {
//                 ({type:errorName, validatorKey: errorCode, message: errorDetail} = element);
//                 errorMessage.push(`${errorName} (${errorCode}): ${errorDetail}`);
//             });
//         }
//         res.status(500).json({
//             errorMessage,
//         });
//     }
// };

/* Login */
// export async function loginMineral(req, res) {
//     try {
//         const { email, password }  = req.body;
//         // Check that account exists
//         const account = await Mineral.findOne({
//             where: {
//                 email
//             }
//         });
//         if (!account) {
//             return res.status(404).json({
//               message: `Mineral email ${email} not found`,
//               data: {}
//             });
//         }
//         // Check if pass is valid
//         const VALIDPASS = await bcrypt.compare(password, account.password);
//         if (!VALIDPASS) {
//             return res.status(404).json({
//                 message: `Mineral password wrong`,
//                 data: {}
//             });
//         }
//         // Logged in
//         const TOKEN = jwt.sign({
//           _id: account.id,
//           email: account.email
//         }, CONFVARS.secret, {
//           expiresIn: '7d'
//         });
//         res.header('auth-token', TOKEN).json({
//             message: 'Logged in',
//             token: TOKEN
//         });
//     } catch (e) {
//         console.log(e);
//     }
// }

/* Delete */
// export async function deleteMineral(req, res) {
//     try {
//         const { id } = req.params;
//         const deleteRowCount = await Mineral.destroy({
//             where: {
//                 id
//             }
//         });
//         if (deleteRowCount > 0) {
//             res.json({
//                 message: `Mineral ID #${id} deleted`,
//                 count: deleteRowCount
//             });
//         } else {
//             res.status(500).json({
//                 message: `Mineral ID #${id} not found`
//             });
//         } 
//     } catch (e) {
//         console.log(e);
//     }
// }


function _getBankAccountType() {
  _getBankAccountType = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(req, res) {
    var id, account;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            id = req.params.id;
            _context2.next = 4;
            return _AccountType.BankAccountType.findOne({
              where: {
                id: id
              }
            });

          case 4:
            account = _context2.sent;

            if (!account) {
              _context2.next = 9;
              break;
            }

            return _context2.abrupt("return", res.json({
              data: account
            }));

          case 9:
            return _context2.abrupt("return", res.status(404).json({
              message: "BankAccountType ID #".concat(id, " not found")
            }));

          case 10:
            _context2.next = 15;
            break;

          case 12:
            _context2.prev = 12;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 12]]);
  }));
  return _getBankAccountType.apply(this, arguments);
}