"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatRut = formatRut;
exports.cleanRut = cleanRut;
exports.checkRut = checkRut;

function formatRut(rut) {
  rut = cleanRut(rut);
  var BODY = rut.slice(0, -1).replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  var VERIFIER = rut.slice(-1).toUpperCase();
  return "".concat(BODY, "-").concat(VERIFIER);
}

function cleanRut(rut) {
  return rut.replace(/\D/g, '').trim();
}

function checkRut(rut) {
  rut = cleanRut(rut); // Aislar Cuerpo y Dígito Verificador

  var BODY = rut.slice(0, -1);
  var verifier = rut.slice(-1).toUpperCase(); // Si no cumple con el mínimo ej. (n.nnn.nnn)

  if (BODY.length < 7) {
    return false;
  } // Calcular Dígito Verificador


  var suma = 0;
  var multiplo = 2; // Para cada dígito del Cuerpo

  for (var i = 1; i <= BODY.length; i++) {
    // Obtener su Producto con el Múltiplo Correspondiente
    var INDEX = multiplo * rut.charAt(BODY.length - i); // Sumar al Contador General

    suma = suma + INDEX; // Consolidar Múltiplo dentro del rango [2,7]

    if (multiplo < 7) {
      multiplo = multiplo + 1;
    } else {
      multiplo = 2;
    }
  } // Calcular Dígito Verificador en base al Módulo 11


  var HOPEDVERIFIER = 11 - suma % 11; // Casos Especiales (0 y K)

  verifier = verifier == 'K' ? 10 : verifier;
  verifier = verifier == 0 ? 11 : verifier; // Validar que el Cuerpo coincide con su Dígito Verificador

  if (HOPEDVERIFIER != verifier) {
    return false;
  }

  return true;
}