"use strict";

var _config = require("../config");

var jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
  var TOKEN = req.header('auth-token');

  if (!TOKEN) {
    return res.status(401).json({
      message: "Access denied"
    });
  }

  try {
    var VERIFIED = jwt.verify(TOKEN, _config.CONFVARS.secret);
    req.account = VERIFIED;
    next();
  } catch (error) {
    res.status(400).json({
      message: "Invalid Token"
    });
  }
};