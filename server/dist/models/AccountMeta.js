"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* Model */
var AccountMeta = _database.sequelize.define('accounts_meta', {
  id: {
    type: _sequelize.default.BIGINT,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: _sequelize.default.STRING(50)
  },
  surname: {
    type: _sequelize.default.STRING(50)
  },
  second_surname: {
    type: _sequelize.default.STRING(50)
  },
  tel: {
    type: _sequelize.default.STRING(20)
  },
  rut: {
    type: _sequelize.default.STRING(50)
  }
}, {
  underscored: true
});

var _default = AccountMeta;
exports.default = _default;