"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* Model */
var BankAccount = _database.sequelize.define('bank_accounts', {
  id: {
    type: _sequelize.default.BIGINT,
    autoIncrement: true,
    primaryKey: true
  },
  bank_account_number: {
    type: _sequelize.default.STRING(50),
    unique: true
  },
  bank_account_rut: {
    type: _sequelize.default.STRING(50),
    unique: true
    /* validate: {
      validateRut(rut) {
        const VALIDRUT = checkRut(rut);
        if (!VALIDRUT) {
          throw new Error('Not valid rut!');
        }
      }
    } */

  }
}, {
  underscored: true
});

var _default = BankAccount;
exports.default = _default;