"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

var _Country = require("./info/Country");

var _AccountMeta = _interopRequireDefault(require("./AccountMeta"));

var _PersonalAddress = _interopRequireDefault(require("./PersonalAddress"));

var _IdDoc = _interopRequireDefault(require("./IdDoc"));

var _CardRequest = _interopRequireDefault(require("./CardRequest"));

var _CardRequestStatus = require("./info/CardRequestStatus");

var _MineralRequest = _interopRequireDefault(require("./MineralRequest"));

var _MineralRequestStatus = require("./info/MineralRequestStatus");

var _Mineral = require("./info/Mineral");

var _MineralPresentation = require("./info/MineralPresentation");

var _Bank = require("./info/Bank");

var _AccountType = require("./info/AccountType");

var _MetalicumPreregister = _interopRequireDefault(require("./MetalicumPreregister"));

var _BankAccount = _interopRequireDefault(require("./BankAccount"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* Model */
var Account = _database.sequelize.define('accounts', {
  id: {
    type: _sequelize.default.BIGINT,
    autoIncrement: true,
    primaryKey: true
  },
  email: {
    type: _sequelize.default.STRING(100),
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true
    }
  },
  password: {
    type: _sequelize.default.STRING(1024),
    allowNull: false,
    validate: {
      notEmpty: true
    }
  },
  reset_password_token: {
    type: _sequelize.default.STRING(1024),
    unique: true
  }
}, {
  underscored: true
});
/* Associations */
// Country


Account.hasOne(_MetalicumPreregister.default, {
  foreignKey: {
    name: "account_id",
    allowNull: false,
    unique: true
  }
});

_Country.Country.hasMany(_AccountMeta.default, {
  foreignKey: {
    name: "country_id",
    allowNull: true
  }
});

_Country.Country.hasMany(_PersonalAddress.default, {
  foreignKey: {
    name: "country_id",
    allowNull: true
  }
});

Account.hasMany(_BankAccount.default, {
  foreignKey: {
    name: "account_id",
    allowNull: false,
    unique: false
  }
});

_Bank.Bank.hasOne(_BankAccount.default, {
  foreignKey: {
    name: "bank_id",
    allowNull: false,
    unique: true
  }
});

_AccountType.BankAccountType.hasOne(_BankAccount.default, {
  foreignKey: {
    name: "bank_account_type_id",
    allowNull: false,
    unique: true
  }
}); // Account Meta


Account.hasOne(_AccountMeta.default, {
  foreignKey: {
    name: "account_id",
    allowNull: false,
    unique: true
  }
});
Account.hasOne(_PersonalAddress.default, {
  foreignKey: {
    name: "account_id",
    allowNull: false,
    unique: true
  }
}); // ID doc

Account.hasOne(_IdDoc.default, {
  foreignKey: {
    name: "account_id",
    allowNull: false,
    unique: true
  }
}); // Card Requests

Account.hasOne(_CardRequest.default, {
  foreignKey: {
    name: "account_id",
    allowNull: false,
    unique: true
  }
});

_CardRequest.default.belongsTo(_CardRequestStatus.CardRequestStatus, {
  foreignKey: {
    name: "status_id",
    allowNull: false
  }
}); // Mineral Requests


Account.hasMany(_MineralRequest.default, {
  foreignKey: {
    name: "account_id",
    allowNull: false
  }
});

_Mineral.Mineral.hasMany(_MineralRequest.default, {
  foreignKey: {
    name: "mineral_id",
    allowNull: false
  }
});

_MineralRequest.default.belongsTo(_Mineral.Mineral, {
  foreignKey: {
    name: "mineral_id",
    allowNull: false
  }
});

_Bank.Bank.hasMany(_MineralRequest.default, {
  foreignKey: {
    name: "bank_id",
    allowNull: false
  }
});
/* MineralRequest.belongsTo(Bank, {
  foreignKey: {
    name: "bank_id",
    allowNull: false
  },
}); */


_AccountType.BankAccountType.hasMany(_MineralRequest.default, {
  foreignKey: {
    name: "bank_account_type_id",
    allowNull: false
  }
});

_MineralRequestStatus.MineralRequestStatus.hasMany(_MineralRequest.default, {
  foreignKey: {
    name: "status_id",
    allowNull: false
  }
});
/* Mineral.belongsTo(MineralPresentation, {
  foreignKey: {
		name: "minerals_presentation_id",
		allowNull: false
	},
}); */

/* Create Tables and set countries */

/* sequelize.sync({
  force: true,
})
.then(() => {
  setCountries();
  setPresentations();
  setMinerals();
  setMineralStatus();
  setCardStatus();
  setBanks();
  setBankAccountTypes();
}); */


var _default = Account;
exports.default = _default;