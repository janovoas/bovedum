"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* Model */
var PersonalAddress = _database.sequelize.define('personal_addresses', {
  id: {
    type: _sequelize.default.BIGINT,
    autoIncrement: true,
    primaryKey: true
  },
  street: {
    type: _sequelize.default.STRING(50)
  },
  number: {
    type: _sequelize.default.STRING(8)
  },
  commune: {
    type: _sequelize.default.STRING(50)
  },
  city: {
    type: _sequelize.default.STRING(50)
  },
  region: {
    type: _sequelize.default.STRING(50)
  }
}, {
  underscored: true
});

var _default = PersonalAddress;
exports.default = _default;