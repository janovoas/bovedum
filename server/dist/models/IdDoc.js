"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

var _rut = require("../services/rut");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* Model */
var IdDoc = _database.sequelize.define('id_docs', {
  id: {
    type: _sequelize.default.BIGINT,
    autoIncrement: true,
    primaryKey: true
  },
  expiration_date: {
    type: _sequelize.default.DATEONLY
  },
  issue_date: {
    type: _sequelize.default.DATEONLY
  },
  doc_number: {
    type: _sequelize.default.STRING(50),
    unique: true,
    validate: {
      validateRut: function validateRut(rut) {
        var VALIDRUT = (0, _rut.checkRut)(rut);

        if (!VALIDRUT) {
          throw new Error('Not valid rut!');
        }
      }
    }
  },
  doc_serial: {
    type: _sequelize.default.STRING(50)
  }
}, {
  underscored: true
});

var _default = IdDoc;
exports.default = _default;