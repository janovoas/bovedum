"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setMinerals = setMinerals;
exports.Mineral = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* Model */
var Mineral = _database.sequelize.define('minerals', {
  id: {
    type: _sequelize.default.SMALLINT,
    autoIncrement: true,
    primaryKey: true
  },
  mineral_name: {
    type: _sequelize.default.STRING(100),
    allowNull: false
  },
  mineral_description: {
    type: _sequelize.default.TEXT
  },
  mineral_image: {
    type: _sequelize.default.STRING(50)
  },
  mineral_value: {
    type: _sequelize.default.FLOAT,
    allowNull: false,
    default: 100
  },
  mineral_type: {
    type: _sequelize.default.STRING(10)
  }
}, {
  timestamps: false,
  underscored: true,
  onDelete: 'SET NULL'
});
/* Set minerals list */


exports.Mineral = Mineral;

function setMinerals() {
  return _setMinerals.apply(this, arguments);
}

function _setMinerals() {
  _setMinerals = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var MINERALS;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            MINERALS = [{
              mineral_name: "Oro físico",
              mineral_description: "Oro certificado 999,9 de pureza. Invertir en oro certificado para ahorrar o asegurar tu patrimonio es uno de los métodos más seguros de inversión ofrecidos por el mercado. El aumento por el valor del oro te permitirán hacer crecer tu patrimonio de una forma segura y no especulativa.",
              mineral_image: "oro@2x.png",
              mineral_value: 49.16,
              mineral_type: "physical"
            }, {
              mineral_name: "Plata física",
              mineral_description: "Plata certificada 999,0 de pureza. Invertir en minerales es la mejor forma de resguardar tu capital. Compra Plata certificada por el LMBA y comienza a ahorrar de forma inteligente.",
              mineral_image: "oro@2x.png",
              mineral_value: 12.4,
              mineral_type: "physical"
            }, {
              mineral_name: "Metalicum",
              mineral_description: "Metalicum es un activo digital estable, solvente y real. El activo digital es respaldado por oro físico certificado. Lanzamiento noviembre 2019. Pre - inscripción necesaria para participar de la oferta inicial.",
              mineral_image: "metalicum-logo@2x.png",
              mineral_value: 0,
              mineral_type: "digital"
            }, {
              mineral_name: "Oro digital en bóveda",
              mineral_description: "El Oro digital en bóveda es un producto pensado para países con inestabilidad jurídica. Queremos que todas las personas tengan acceso a proteger su patrimonio. Ofrecemos el servicio de guarda costo 0 $ por el primer año para los países en la lista.El costo de almacenamiento para los otros países será el estándar del mercado. Resguarda tu capital en oro, tu mineral estará seguro en nuestra Bóveda.Puedes acceder a tu certificado de propiedad digital en todo momento.",
              mineral_image: "oro@2x.png",
              mineral_value: 20.03,
              mineral_type: "digital"
            }];
            _context.next = 3;
            return Mineral.bulkCreate(MINERALS, {
              validate: true,
              ignoreDuplicates: true
            });

          case 3:
            return _context.abrupt("return", _context.sent);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _setMinerals.apply(this, arguments);
}