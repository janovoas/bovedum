"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setBankAccountTypes = setBankAccountTypes;
exports.BankAccountType = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* Model */
var BankAccountType = _database.sequelize.define('bank_account_types', {
  id: {
    type: _sequelize.default.BIGINT,
    autoIncrement: true,
    primaryKey: true
  },
  account_type: {
    type: _sequelize.default.STRING(100)
  }
}, {
  timestamp: false,
  underscored: true
});
/* Set countries list */


exports.BankAccountType = BankAccountType;

function setBankAccountTypes() {
  return _setBankAccountTypes.apply(this, arguments);
}

function _setBankAccountTypes() {
  _setBankAccountTypes = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var fs, rawdata, BANKS;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            fs = require('fs');
            rawdata = fs.readFileSync(__dirname + '/accounttypes.json', 'utf8');
            BANKS = JSON.parse(rawdata);
            _context.next = 5;
            return BankAccountType.bulkCreate(BANKS, {
              validate: true,
              ignoreDuplicates: true
            });

          case 5:
            return _context.abrupt("return", _context.sent);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _setBankAccountTypes.apply(this, arguments);
}