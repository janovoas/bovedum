"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setCountries = setCountries;
exports.Country = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* Model */
var Country = _database.sequelize.define('countries', {
  id: {
    type: _sequelize.default.SMALLINT,
    autoIncrement: true,
    primaryKey: true
  },
  country_name: {
    type: _sequelize.default.STRING(100),
    allowNull: false
  },
  country_code: {
    type: _sequelize.default.STRING(2),
    unique: true,
    allowNull: false
  }
}, {
  timestamps: false,
  underscored: true,
  onDelete: 'SET NULL'
});
/* Set countries list */


exports.Country = Country;

function setCountries() {
  return _setCountries.apply(this, arguments);
}

function _setCountries() {
  _setCountries = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var fs, rawdata, COUNTRIES;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            fs = require('fs');
            rawdata = fs.readFileSync(__dirname + '/countries.json', 'utf8');
            COUNTRIES = JSON.parse(rawdata);
            _context.next = 5;
            return Country.bulkCreate(COUNTRIES, {
              validate: true,
              ignoreDuplicates: true
            });

          case 5:
            return _context.abrupt("return", _context.sent);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _setCountries.apply(this, arguments);
}