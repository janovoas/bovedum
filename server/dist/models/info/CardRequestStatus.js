"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setCardStatus = setCardStatus;
exports.CardRequestStatus = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* Model */
var CardRequestStatus = _database.sequelize.define('card_request_statuses', {
  id: {
    type: _sequelize.default.SMALLINT,
    autoIncrement: true,
    primaryKey: true
  },
  status: {
    type: _sequelize.default.STRING(20),
    allowNull: false
  }
}, {
  timestamps: false,
  underscored: true,
  onDelete: 'SET NULL'
});
/* Set countries list */


exports.CardRequestStatus = CardRequestStatus;

function setCardStatus() {
  return _setCardStatus.apply(this, arguments);
}

function _setCardStatus() {
  _setCardStatus = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var STATUS;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            STATUS = [{
              status: "Pagado"
            }, {
              status: "Procesando"
            }, {
              status: "Entregada"
            }];
            _context.next = 3;
            return CardRequestStatus.bulkCreate(STATUS, {
              validate: true,
              ignoreDuplicates: true
            });

          case 3:
            return _context.abrupt("return", _context.sent);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _setCardStatus.apply(this, arguments);
}