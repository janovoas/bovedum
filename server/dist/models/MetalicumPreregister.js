"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* Model */
var MetalicumPreregister = _database.sequelize.define('metalicum_preregister', {
  id: {
    type: _sequelize.default.BIGINT,
    autoIncrement: true,
    primaryKey: true
  }
}, {
  underscored: true
});

var _default = MetalicumPreregister;
exports.default = _default;