"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* Model */
var MineralRequest = _database.sequelize.define('mineral_requests', {
  id: {
    type: _sequelize.default.BIGINT,
    autoIncrement: true,
    primaryKey: true
  },
  address: {
    type: _sequelize.default.STRING(50),
    required: true
  },
  address_number: {
    type: _sequelize.default.STRING(10),
    required: true
  },
  city: {
    type: _sequelize.default.STRING(10),
    required: true
  },
  country: {
    type: _sequelize.default.STRING(10),
    required: true
  },
  zip: {
    type: _sequelize.default.STRING(10),
    required: true
  },
  mineral_presentation: {
    type: _sequelize.default.STRING(30),
    required: true
  },
  bank_account_number: {
    type: _sequelize.default.STRING(30),
    required: true
  },
  bank_account_rut: {
    type: _sequelize.default.STRING(30),
    required: true
  },
  total_value: {
    type: _sequelize.default.STRING(30),
    required: true
  },
  payment_type: {
    type: _sequelize.default.STRING(30),
    required: true
  }
}, {
  underscored: true
});

var _default = MineralRequest;
exports.default = _default;