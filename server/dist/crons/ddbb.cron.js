"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteTenMnts = deleteTenMnts;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _MineralRequest = _interopRequireDefault(require("../models/MineralRequest"));

var _Account = _interopRequireDefault(require("../models/Account"));

var _sendgrid = require("../mail/sendgrid");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var Op = _sequelize.default.Op;

var moment = require('moment');

var CronJob = require('cron').CronJob;

var shell = require('shelljs');

var ENV = process.env.ENV || 'dev';

function parseRequests() {
  return _parseRequests.apply(this, arguments);
}

function _parseRequests() {
  _parseRequests = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var requests, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, request, id, REQID, ACCOUNT, EMAIL, msg, updateMineralRequest;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _MineralRequest.default.findAll({
              where: {
                created_at: _defineProperty({}, Op.lt, moment().subtract(10, 'minutes').toDate()),
                status_id: 1
              },
              attributes: ['account_id', 'id']
            });

          case 3:
            requests = _context.sent;

            if (!requests) {
              _context.next = 46;
              break;
            }

            _iteratorNormalCompletion = true;
            _didIteratorError = false;
            _iteratorError = undefined;
            _context.prev = 8;
            _iterator = requests[Symbol.iterator]();

          case 10:
            if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
              _context.next = 32;
              break;
            }

            request = _step.value;
            id = request.account_id;
            REQID = request.id;
            _context.prev = 14;
            _context.next = 17;
            return _Account.default.findOne({
              where: {
                id: id
              },
              attributes: ['email']
            });

          case 17:
            ACCOUNT = _context.sent;

            if (!ACCOUNT) {
              _context.next = 24;
              break;
            }

            EMAIL = ACCOUNT.email;

            if (ENV === 'prod') {
              msg = {
                to: EMAIL,
                from: 'Bovedum<contacto@bovedum.com>',
                templateId: 'd-279a8db2381144e386aeda52494251ff',
                dynamic_template_data: {
                  REQID: REQID
                }
              };
              (0, _sendgrid.sendMail)(msg);
            }

            _context.next = 23;
            return _MineralRequest.default.update({
              status_id: 4
            }, {
              where: {
                id: REQID
              }
            });

          case 23:
            updateMineralRequest = _context.sent;

          case 24:
            _context.next = 29;
            break;

          case 26:
            _context.prev = 26;
            _context.t0 = _context["catch"](14);
            console.error(_context.t0);

          case 29:
            _iteratorNormalCompletion = true;
            _context.next = 10;
            break;

          case 32:
            _context.next = 38;
            break;

          case 34:
            _context.prev = 34;
            _context.t1 = _context["catch"](8);
            _didIteratorError = true;
            _iteratorError = _context.t1;

          case 38:
            _context.prev = 38;
            _context.prev = 39;

            if (!_iteratorNormalCompletion && _iterator.return != null) {
              _iterator.return();
            }

          case 41:
            _context.prev = 41;

            if (!_didIteratorError) {
              _context.next = 44;
              break;
            }

            throw _iteratorError;

          case 44:
            return _context.finish(41);

          case 45:
            return _context.finish(38);

          case 46:
            _context.next = 51;
            break;

          case 48:
            _context.prev = 48;
            _context.t2 = _context["catch"](0);
            console.error(_context.t2);

          case 51:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 48], [8, 34, 38, 46], [14, 26], [39,, 41, 45]]);
  }));
  return _parseRequests.apply(this, arguments);
}

function deleteTenMnts() {
  return _deleteTenMnts.apply(this, arguments);
}

function _deleteTenMnts() {
  _deleteTenMnts = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2() {
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            new CronJob('*/2 * * * *', function () {
              parseRequests();
            }, null, true, 'America/Santiago');

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _deleteTenMnts.apply(this, arguments);
}