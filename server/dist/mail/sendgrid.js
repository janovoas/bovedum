"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sendMail = sendMail;

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var sgMail = require('@sendgrid/mail');

var path = require('path');

require('dotenv').config({
  path: path.resolve(__dirname, '../../../.env')
});

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

function sendMail(msg) {
  sgMail.send(msg).then(function (_ref) {// console.log(response);
    // console.log(body);

    var _ref2 = _slicedToArray(_ref, 2),
        response = _ref2[0],
        body = _ref2[1];
  }).catch(function (e) {
    return console.log(e);
  });
}